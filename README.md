# Movie Explorer Max

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Things to Know

I made the functions clear and self explanatory and so as to minimise the amount of comments needed and to simplify the readthrough of the code (this is a universal best practice)

For the sake of this exercise I’ve lest some explanatory comments. To find them just search the projects for any comments starting with ``// INFO``, this is just to give some helpful guides and explain the reasoning for the code.
I went with Bootstrap because it's really fast to well “bootstrap” the UI and it uses simple components and is easy to test. I prefere Material but time is a constraint here so I think it will do.

I used a Redux store (as requested) more specifically NGRX version 7+ (new one), Also I’ve used a serializer/deserialize pattern since it helps keep the API modeling clear and efficient and it also guarantees some precise patterns in the resulting Objects regardless of the API response.
I’ve used Interceptors to add the API key, Enums and STRONG typing all round with nominal exceptions, and I aimed for excellent code coverage,
I’ve tested everything from Reducers, Models, Selectors, Components, Services, Interceptor, Store, you name it. I could have even done E2E tests with Protractor but again time is a constraint here.

Apologies in advance for any typos in the text  or code :)

