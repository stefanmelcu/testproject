
export const CONFIG: any = {
  APIS: {
    MOVIE_API: {
      BASE_URL: 'https://api.themoviedb.org/3',
      IMAGE_RESOURCES_URL: 'https://image.tmdb.org/t/p',
      PARAMS: {
        API_KEY: 'api_key',
        SORT_BY: 'sort_by',
        PAGE_KEY: 'page',
        INCLUDE_ADULT: 'include_adult',
        YEAR: 'year'
      },
      PATHS: {
        DISCOVER: '/discover/movie',
        DETAILS: '/movie/'
      }
    },
  },
  ROUTES: {
    NOT_FOUND: {
      SLUG: '404'
    },
    HOME: {
      SLUG: '',
    },
    MOVIE: {
      SLUG: 'movie/:id'
    }
  },
  KEYS: {
    // INFO its a public key so no one should be scared of putting it here. + movieDB api can restrict the Origin so no CORS problems
    MOVIE_DB_KEY: 'd7d9992e1c4a0fb128c166a180b7ae84'
  }
};
