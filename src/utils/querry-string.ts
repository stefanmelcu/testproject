export function toQueryString(keyValuePair: any = {}): string {
    return Object.keys(keyValuePair).map(key => key + '=' + keyValuePair[key]).join('&');
}

export function toEncodedQueryString(keyValuePair: any = {}): string {
  return Object.keys(keyValuePair).map((key) => {
    return encodeURIComponent(key) + '=' + encodeURIComponent(keyValuePair[key])
  }).join('&');
}


export function appendQueryStringToUrl(url: string = '', params: string = ''): string {
  const urlObject: URL = new URL(url);
  const originalParams: URLSearchParams = new URLSearchParams(urlObject.search);
  const extraParams: URLSearchParams = new URLSearchParams(params);
  extraParams.forEach((value: string, key: string) => {
    originalParams.append(key, value);
  });

  return `${urlObject.origin}${urlObject.pathname}${originalParams.toString() && `?${originalParams.toString()}` }`;
}
