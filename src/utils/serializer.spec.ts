import {JsonConvert} from 'json2typescript';
import { serialize } from './serializer';

describe('serializer', () => {
    it('serialize function should exist', () => {
        expect(serialize).toBeTruthy();
    });

    it('serialize function should call the jsonConvert.serialize and attempt to serialize the object', () => {
        const serializeSpy = spyOn(JsonConvert.prototype, 'serialize');
        serialize('{}',);
        expect(serializeSpy).toHaveBeenCalled();
    });
});
