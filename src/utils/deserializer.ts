import {JsonConvert, OperationMode, ValueCheckingMode} from 'json2typescript';
import {throwError} from 'rxjs';

export interface DeserializeOptions {
    /* Log the deserialization process or not */
    debug?: boolean;
    /* Allow nulls int he deserialization or not */
    allowNull?: ValueCheckingMode;
    /* don't allow assigning of numbers to string etc. */
    ignorePrimitiveChecks?: boolean;
}

const defaultDeserializeOptions: DeserializeOptions = {
    debug: false,
    allowNull: ValueCheckingMode.ALLOW_NULL,
    ignorePrimitiveChecks: false
};

export function deserialize<T>(
    json: {} | null,
    classReference: new () => {},
    options: DeserializeOptions = defaultDeserializeOptions
): T {
    const finalOptions = Object.assign({}, defaultDeserializeOptions, options);

    const jsonConvert: JsonConvert = new JsonConvert();
    jsonConvert.operationMode = finalOptions.debug ? OperationMode.LOGGING : OperationMode.ENABLE;
    jsonConvert.ignorePrimitiveChecks = !!finalOptions.ignorePrimitiveChecks;
    jsonConvert.valueCheckingMode = finalOptions.allowNull ? finalOptions.allowNull : ValueCheckingMode.ALLOW_NULL;

    try {
        return jsonConvert.deserialize(json, classReference) as T;
    } catch (e) {
        console.log(`Deserialization failed`, (e as Error));
        throwError(e as Error);
    }

    return jsonConvert.deserialize({}, classReference) as T;
}
