import {toEncodedQueryString, toQueryString} from './querry-string';

describe('Quarry Strings', () => {
    it('toQueryString function should exist', () => {
        expect(toQueryString).toBeTruthy();
    });

    it('toQueryString function should correctly format a object string', () => {
        expect(toQueryString({
            a: 1,
            b: 'c',
            d: 33
        })).toBe('a=1&b=c&d=33');
    });


    it('toEncodedQueryString function should exist', () => {
        expect(toEncodedQueryString).toBeTruthy();
    });

    it('toEncodedQueryString function should correctly format a object string and encode the output in a url safe format', () => {
        expect(toEncodedQueryString({
            'MÜLLER':  '读写汉字 - 学中文',
            aaa: 333
        })).toBe('M%C3%9CLLER=%E8%AF%BB%E5%86%99%E6%B1%89%E5%AD%97%20-%20%E5%AD%A6%E4%B8%AD%E6%96%87&aaa=333');
    });
});
