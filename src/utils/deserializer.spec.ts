import { JsonConvert } from 'json2typescript';
import { deserialize } from './deserializer';

describe('deserializer', () => {
    it('deserialize function should exist', () => {
        expect(deserialize).toBeTruthy();
    });

    it('deserialize function should call the jsonConvert.deserialize and attempt to deserialize the object', () => {
        const deserializeSpy = spyOn(JsonConvert.prototype, 'deserialize');
        deserialize('{}', Object);
        expect(deserializeSpy).toHaveBeenCalled();
    });
});
