import {JsonConvert, OperationMode, ValueCheckingMode} from 'json2typescript';
import {throwError} from 'rxjs';

export interface SerializeOptions {
    /* Log the deserialization process or not */
    debug?: boolean;
    /* Allow nulls int he deserialization or not */
    allowNull?: ValueCheckingMode;
   /* don't allow assigning of numbers to string etc. */
    ignorePrimitiveChecks?: boolean;
}

const defaultSerializeOptions: SerializeOptions = {
    debug: false,
    allowNull: ValueCheckingMode.ALLOW_NULL,
    ignorePrimitiveChecks: false
};

export function serialize(
    instance: any,
    options: SerializeOptions = defaultSerializeOptions
): any {
    const finalOptions = Object.assign({}, defaultSerializeOptions, options);

    let jsonConvert: JsonConvert = new JsonConvert();
    jsonConvert.operationMode = finalOptions.debug ? OperationMode.LOGGING : OperationMode.ENABLE;
    jsonConvert.ignorePrimitiveChecks = !!finalOptions.ignorePrimitiveChecks;
    jsonConvert.valueCheckingMode = finalOptions.allowNull ? finalOptions.allowNull : ValueCheckingMode.ALLOW_NULL;

    try {
        return jsonConvert.serialize(instance);
    } catch (e) {
        console.log(`Serialization failed`, (e as Error));
        throwError(e as Error);
    }
}
