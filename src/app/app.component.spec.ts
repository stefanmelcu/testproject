import {TestBed, async, ComponentFixture} from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let component: AppComponent;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create the app', () => {
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have the site logo visible at all times`, () => {
    expect(fixture.nativeElement.querySelector('[data-test="siteLogo"]')).toBeTruthy();
  });

  it('should have the correct title visible at all time', () => {
    expect(fixture.nativeElement.querySelector('[data-test="siteTitle"]').textContent).toContain(component.title);
  });
});
