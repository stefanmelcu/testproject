import {JsonConverter, JsonCustomConvert} from 'json2typescript';

// INFO takes a string date and makes it a OBJECT Date
// INFO Convertes are great to add things like time zone differences
// or manipulate stuff right as they come off the API response
@JsonConverter
export class ISODateConverter implements JsonCustomConvert<Date> {
  serialize(date: Date): string {
    return date.toISOString();
  }

  deserialize(date: string): Date {
    return date ? new Date(date) : (undefined as unknown as Date);
  }
}
