import { ISODateConverter } from './iso-date.convertor';

describe('ISODateConverter', () => {
  let converter: ISODateConverter;

  beforeEach(() => {
    converter = new ISODateConverter();
  });

  it('ISODateConverter function should exist', () => {
    expect(ISODateConverter).toBeTruthy();
  });

  it('serialize method should correctly generate a ISODateString', () => {
    expect(converter.serialize(new Date('2020-03-04'))).toBe('2020-03-04T00:00:00.000Z');
  });

  it('deserialize method should correctly generate a Date from a isoString', () => {
    expect(converter.deserialize('2020-03-04T00:00:00.000Z')).toEqual(jasmine.any(Date));
  });
});
