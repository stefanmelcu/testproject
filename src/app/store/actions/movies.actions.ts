import {createAction, props} from '@ngrx/store';
import {MoviesFilter} from '../../models/movies';
import DiscoverMovieResponse from '../../models/movies/discover-movie-response.model';
import DetailsMovieResponse from '../../models/movies/details-movie-response.model';

export const loadMoviesAction = createAction(
  '[MoviesPage Component] Load Movies request',
  props<{ filters: MoviesFilter }>()
);

export const loadMoviesSuccessAction = createAction(
  '[MoviesDBService Service] Load Movies success',
  props<{ response: DiscoverMovieResponse }>()
);

export const loadMoviesErrorAction = createAction(
  '[MoviesDBService Service] Load Movies error',
  props<{ error: string }>()
);

export const loadMovieAction = createAction(
  '[MovieCheckGuard Guard] Load Movie request',
  props<{ id: number }>()
);

export const loadMovieSuccessAction = createAction(
  '[MovieCheckGuard Guard] Load Movie success',
  props<{ response: DetailsMovieResponse }>()
);

export const loadMovieErrorAction = createAction(
  '[MovieCheckGuard Guard] Load Movie error',
  props<{ error: string }>()
);
