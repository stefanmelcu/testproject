import {
  loadMovieAction,
  loadMovieErrorAction,
  loadMoviesAction,
  loadMoviesErrorAction,
  loadMoviesSuccessAction,
  loadMovieSuccessAction,
} from './movies.actions';
import DiscoverMovieResponse from '../../models/movies/discover-movie-response.model';
import DetailsMovieResponse from '../../models/movies/details-movie-response.model';

describe('MoviesActions', () => {
  it('should create an action loadMoviesAction', () => {
    const action = loadMoviesAction({ filters: { test: true }  });

    expect({ ...action }).toEqual({
      type: '[MoviesPage Component] Load Movies request',
      filters: {
        test: true
      },
    });
  });

  it('should create an action loadMoviesSuccessAction', () => {
    const payload: DiscoverMovieResponse = new DiscoverMovieResponse();
    payload.totalPages = 333;

    const action = loadMoviesSuccessAction({ response: payload  });

    const { response, type } =  action;
    expect(type).toEqual('[MoviesDBService Service] Load Movies success');
    expect(response?.totalPages).toEqual(333);
  });

  it('should create an action loadMoviesErrorAction', () => {
    const action = loadMoviesErrorAction({ error: 'bad'  });

    const { error, type } =  action;
    expect(type).toEqual('[MoviesDBService Service] Load Movies error');
    expect(error).toEqual('bad');
  });

  it('should create an action loadMovieAction', () => {
    const action = loadMovieAction({ id: 444  });

    expect({ ...action }).toEqual({
      type: '[MovieCheckGuard Guard] Load Movie request',
      id: 444,
    });
  });

  it('should create an action loadMovieSuccessAction', () => {
    const payload: DetailsMovieResponse = new DetailsMovieResponse();
    payload.id = 222;

    const action = loadMovieSuccessAction({ response: payload  });

    const { response, type } =  action;
    expect(type).toEqual('[MovieCheckGuard Guard] Load Movie success');
    expect(response?.id).toEqual(222);
  });

  it('should create an action loadMovieErrorAction', () => {
    const action = loadMovieErrorAction({ error: 'bad'  });

    const { error, type } =  action;
    expect(type).toEqual('[MovieCheckGuard Guard] Load Movie error');
    expect(error).toEqual('bad');
  });
});
