import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {of} from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { MoviesDBService } from '../../services/movies-db.service';
import {
  loadMovieAction, loadMovieErrorAction,
  loadMoviesAction,
  loadMoviesErrorAction,
  loadMoviesSuccessAction,
  loadMovieSuccessAction,
} from '../actions/movies.actions';
import DiscoverMovieResponse from '../../models/movies/discover-movie-response.model';
import DetailsMovieResponse from '../../models/movies/details-movie-response.model';

@Injectable()
export class MoviesEffects {

  loadMovies$ = createEffect(() => this.actions$.pipe(
    ofType(loadMoviesAction),
    mergeMap((action) => this.service.getMovies(action.filters)
      .pipe(
        map((response: DiscoverMovieResponse) => loadMoviesSuccessAction({ response }) ),
        catchError((error: string) => of(loadMoviesErrorAction({ error }) ))
      ))
    )
  );

  loadMovie$ = createEffect(() => this.actions$.pipe(
    ofType(loadMovieAction),
    mergeMap((action) => this.service.getMovie(action.id)
      .pipe(
        map((response: DetailsMovieResponse) => loadMovieSuccessAction({ response }) ),
        catchError((error: string) => of(loadMovieErrorAction({ error }) ))
      ))
    )
  );

  constructor(
    private actions$: Actions,
    private service: MoviesDBService
  ) {}
}
