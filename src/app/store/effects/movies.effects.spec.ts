
import { Observable, of } from 'rxjs';
import { Action } from '@ngrx/store';
import { TestBed, async } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { MoviesEffects } from './movies.effects';
import * as moviesActions from '../actions/movies.actions';
import { MoviesDBService } from '../../services/movies-db.service';
import DetailsMovieResponse from '../../models/movies/details-movie-response.model';
import DiscoverMovieResponse from '../../models/movies/discover-movie-response.model';

describe('MoviesEffects', () => {
  let actions$: Observable<Action>;
  let effects: MoviesEffects;
  let moviesDBService: MoviesDBService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        MoviesEffects,
        provideMockStore(),
        provideMockActions(() => actions$),
        {
          provide: MoviesDBService,
          useValue: jasmine.createSpyObj('MoviesDBService', [ 'getMovies', 'getMovie' ])
        }
      ],
    });

    effects = TestBed.inject<MoviesEffects>(MoviesEffects);
    moviesDBService = TestBed.inject<MoviesDBService>(MoviesDBService);
  }));

  it('should dispatch loadMovieSuccessAction action when a correct loadMovieAction action is dispatched', () => {
    const fakeResponse: DetailsMovieResponse = new DetailsMovieResponse();
    fakeResponse.id = 333;

    (moviesDBService.getMovie as any).and.returnValue(of(fakeResponse));

    actions$ = of(moviesActions.loadMovieAction);

    effects.loadMovie$.subscribe(action => {
      expect(action.type).toBe('[MovieCheckGuard Guard] Load Movie success');
      expect((action as any)?.response?.id).toBe(333);
    });
  });

  it('should dispatch loadMovieErrorAction action when a invalid loadMovieAction action is dispatched and fails', () => {
    (moviesDBService.getMovie as any).and.throwError('really bad error');

    actions$ = of(moviesActions.loadMovieAction);

    effects.loadMovie$.subscribe(action => {
      expect(action.type).toBe('[MovieCheckGuard Guard] Load Movie error');
    }, error => {
     expect(error.toString()).toBe('Error: really bad error');
    });
  });

  it('should dispatch loadMovieSuccessAction action when a correct loadMoviesAction action is dispatched', () => {
    const fakeResponse: DiscoverMovieResponse = new DiscoverMovieResponse();
    fakeResponse.id = 333;

    (moviesDBService.getMovies as any).and.returnValue(of(fakeResponse));

    actions$ = of(moviesActions.loadMoviesAction);

    effects.loadMovie$.subscribe(action => {
      expect(action.type).toBe('[MoviesDBService Service] Load Movies success');
      expect((action as any)?.response?.id).toBe(333);
    });
  });

  it('should dispatch loadMovieErrorAction action when a invalid loadMovieAction action is dispatched and fails', () => {
    (moviesDBService.getMovies as any).and.throwError('really bad error2');

    actions$ = of(moviesActions.loadMoviesAction);

    effects.loadMovie$.subscribe(action => {
      expect(action.type).toBe('[MoviesDBService Service] Load Movies error');
    }, error => {
      expect(error.toString()).toBe('Error: really bad error2');
    });
  });
});

