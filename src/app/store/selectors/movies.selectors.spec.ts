import * as selectors from './movies.selectors';
import {movieDetailsResponseKey, moviesListResponseKey, RootStoreState} from '../reducers';
import {MoviesState, MoviesStatePhaseEnum} from '../reducers/movies.reducers';
import {MovieDetailsState, MovieDetailsStatePhaseEnum} from '../reducers/movie.reducers';
import DetailsMovieResponse from '../../models/movies/details-movie-response.model';
import DiscoverMovieResponse from '../../models/movies/discover-movie-response.model';

const crudeInitialDetailsState: MovieDetailsState = {
  moviePhase: MovieDetailsStatePhaseEnum.INIT,
  movieDetails: new DetailsMovieResponse(),
  error: 'baaaad'
};

const createMovieDetailsState = (): MovieDetailsState => (crudeInitialDetailsState);

const crudeInitialState: MoviesState = {
  moviesPhase: MoviesStatePhaseEnum.INIT,
  moviesListResponse: new DiscoverMovieResponse(),
  error: 'good'
};

const createMoviesListState = (): MoviesState => (crudeInitialState);

const createRootState = (): RootStoreState => ({
  [moviesListResponseKey]: createMoviesListState(),
  [movieDetailsResponseKey]: createMovieDetailsState()
});

describe('MoviesSelectors', () => {
  it('selectMoviesList', () => {
    const state = createRootState();
    expect(selectors.selectMoviesList(state)).toBe(crudeInitialState);
  });

  it('selectMovieDetails', () => {
    const state = createRootState();
    expect(selectors.selectMoviesListPhase(state)).toBe(MoviesStatePhaseEnum.INIT);
  });

  it('selectMoviesListError', () => {
    const state = createRootState();
    expect(selectors.selectMoviesListError(state)).toBe('good');
  });
});
