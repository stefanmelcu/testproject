import {movieDetailsResponseKey, RootStoreState} from '../reducers';
import {createFeatureSelector, createSelector} from '@ngrx/store';
import {MovieDetailsState} from '../reducers/movie.reducers';

// INFO I would ideally make a selector for everything since
// it really helps with testing and mocking the store process
export const selectMovieDetailsState = createFeatureSelector<RootStoreState, MovieDetailsState>(movieDetailsResponseKey);
export const selectMovieDetails = createSelector(selectMovieDetailsState, (state: MovieDetailsState) => state.movieDetails);
export const selectMovieDetailsError = createSelector(selectMovieDetailsState, (state: MovieDetailsState) => state.error);
export const selectMovieDetailsPhase = createSelector(selectMovieDetailsState, (state: MovieDetailsState) => state.moviePhase);
