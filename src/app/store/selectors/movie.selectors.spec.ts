import * as selectors from './movie.selectors';
import {movieDetailsResponseKey, moviesListResponseKey, RootStoreState} from '../reducers';
import {MoviesState, MoviesStatePhaseEnum} from '../reducers/movies.reducers';
import {MovieDetailsState, MovieDetailsStatePhaseEnum} from '../reducers/movie.reducers';
import DetailsMovieResponse from '../../models/movies/details-movie-response.model';
import DiscoverMovieResponse from '../../models/movies/discover-movie-response.model';

const crudeInitialDetailsState: MovieDetailsState = {
  moviePhase: MovieDetailsStatePhaseEnum.INIT,
  movieDetails: new DetailsMovieResponse(),
  error: 'baaaad'
};

const createMovieDetailsState = (): MovieDetailsState => (crudeInitialDetailsState);

const crudeInitialState: MoviesState = {
  moviesPhase: MoviesStatePhaseEnum.INIT,
  moviesListResponse: new DiscoverMovieResponse(),
  error: 'good'
};

const createMoviesListState = (): MoviesState => (crudeInitialState);

const createRootState = (): RootStoreState => ({
  [moviesListResponseKey]: createMoviesListState(),
  [movieDetailsResponseKey]: createMovieDetailsState()
});

describe('MovieSelectors', () => {
  it('selectMovieDetailsState', () => {
    const state = createRootState();
    expect(selectors.selectMovieDetailsState(state)).toBe(crudeInitialDetailsState);
  });

  it('selectMovieDetails', () => {
    const state = createRootState();
    expect(selectors.selectMovieDetails(state)).toBe(crudeInitialDetailsState.movieDetails);
  });

  it('selectMovieDetailsError', () => {
    const state = createRootState();
    expect(selectors.selectMovieDetailsError(state)).toBe('baaaad');
  });

  it('selectMovieDetailsPhase', () => {
    const state = createRootState();
    expect(selectors.selectMovieDetailsPhase(state)).toBe(MovieDetailsStatePhaseEnum.INIT);
  });
});
