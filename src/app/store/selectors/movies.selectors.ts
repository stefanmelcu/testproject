import {moviesListResponseKey, RootStoreState} from '../reducers';
import {createFeatureSelector, createSelector} from '@ngrx/store';
import {MoviesState} from '../reducers/movies.reducers';

export const selectMoviesList = createFeatureSelector<RootStoreState, MoviesState>(moviesListResponseKey);
export const selectMoviesListPhase = createSelector(selectMoviesList, (state: MoviesState) => state.moviesPhase);
export const selectMoviesListError = createSelector(selectMoviesList, (state: MoviesState) => state.error);
