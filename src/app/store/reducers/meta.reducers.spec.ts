import {debug} from './meta.reducers';
import {loadMoviesAction} from '../actions/movies.actions';
import {Action, ActionReducer} from '@ngrx/store';

describe('metaReducers', () => {
  it('should correctly bypass all actions ( and log them )', () => {
    const action = loadMoviesAction({ filters: {}});
    const spy = jasmine.createSpy();

    debug(spy as ActionReducer<{}, Action>)({ id: 'test' }, action);

    expect(spy).toHaveBeenCalledWith({ id: 'test'}, action);
  });
});
