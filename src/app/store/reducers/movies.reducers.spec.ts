import {initialState, MoviesState, MoviesStatePhaseEnum} from './movies.reducers';
import {loadMoviesAction, loadMoviesErrorAction, loadMoviesSuccessAction} from '../actions/movies.actions';
import * as fromMovies from './movies.reducers';
import DiscoverMovieResponse from '../../models/movies/discover-movie-response.model';

describe('moviesReducer', () => {
  it('should set moviesPhase to INIT on initialization', () => {
    const initialStateMoviesState: MoviesState = initialState;

    expect(initialStateMoviesState.moviesPhase).toEqual(MoviesStatePhaseEnum.INIT);
    expect(initialStateMoviesState.error).toEqual('');
    expect(initialStateMoviesState.moviesListResponse).toEqual(undefined);
  });

  it('should set moviesPhase to LOADING on loadMoviesAction', () => {
    const initialStateMoviesState: MoviesState = initialState;
    const action = loadMoviesAction({ filters: {}});
    const state = fromMovies.reducer(initialStateMoviesState, action);

    expect(state.moviesPhase).toEqual(MoviesStatePhaseEnum.LOADING);
    expect(state.error).toEqual('');
    expect(state.moviesListResponse).toEqual(undefined);
  });

  it('should set  moviesPhase to LOADING_ERROR and update error on loadMoviesErrorAction', () => {
    const initialStateMoviesState: MoviesState = initialState;
    const action = loadMoviesErrorAction({ error: 'bad bad bad' });
    const state = fromMovies.reducer(initialStateMoviesState, action);

    expect(state.moviesPhase).toEqual(MoviesStatePhaseEnum.LOADING_ERROR);
    expect(state.error).toEqual('bad bad bad');
    expect(state.moviesListResponse).toBeFalsy();
  });

  it('should set  moviesPhase to LOADING_SUCCESSFUL and update  moviesListResponse on loadMoviesSuccessAction', () => {
    const response: DiscoverMovieResponse = new DiscoverMovieResponse();

    const initialStateMoviesState: MoviesState = initialState;
    const action = loadMoviesSuccessAction({ response });
    const state = fromMovies.reducer(initialStateMoviesState, action);

    expect(state.moviesPhase).toEqual(MoviesStatePhaseEnum.LOADING_SUCCESSFUL);
    expect(state.error).toEqual('');
    expect(state.moviesListResponse).not.toBeFalsy();
  });
});
