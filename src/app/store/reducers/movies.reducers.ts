import {
  Action,
  createReducer,
  on,
} from '@ngrx/store';
import * as MoviesActions from '../actions/movies.actions';
import DiscoverMovieResponse from '../../models/movies/discover-movie-response.model';

export enum MoviesStatePhaseEnum {
  'INIT',
  'LOADING',
  'LOADING_SUCCESSFUL',
  'LOADING_ERROR',
}

export interface MoviesState {
  moviesPhase: MoviesStatePhaseEnum;
  moviesListResponse: DiscoverMovieResponse | undefined;
  error: string;
}

export const initialState: MoviesState = {
  moviesPhase: MoviesStatePhaseEnum.INIT,
  moviesListResponse: undefined,
  error: ''
};

const moviesReducer = createReducer(
  initialState,
  on(MoviesActions.loadMoviesAction, state => ({
    ...state,
    moviesPhase: MoviesStatePhaseEnum.LOADING,
  })),
  on(MoviesActions.loadMoviesSuccessAction, (state, action) => ({
    ...state,
    moviesPhase: MoviesStatePhaseEnum.LOADING_SUCCESSFUL,
    moviesListResponse: action.response,
    error: ''
  })),
  on(MoviesActions.loadMoviesErrorAction, (state, action) => ({
    ...state,
    moviesPhase: MoviesStatePhaseEnum.LOADING_ERROR,
    moviesListResponse: undefined,
    error: action.error
  })),
);

export function reducer(state: MoviesState | undefined, action: Action): MoviesState {
  return moviesReducer(state, action);
}
