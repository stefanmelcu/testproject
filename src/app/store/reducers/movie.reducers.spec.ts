import {initialState, MovieDetailsState, MovieDetailsStatePhaseEnum} from './movie.reducers';
import {
  loadMovieAction,
  loadMovieErrorAction,
  loadMovieSuccessAction,
} from '../actions/movies.actions';
import * as fromMovie from './movie.reducers';
import DetailsMovieResponse from '../../models/movies/details-movie-response.model';

describe('movieReducer', () => {
  it('should set moviePhase to INIT on initialization', () => {
    const initialStateMovieState: MovieDetailsState = initialState;

    expect(initialStateMovieState.moviePhase).toEqual(MovieDetailsStatePhaseEnum.INIT);
    expect(initialStateMovieState.error).toEqual('');
    expect(initialStateMovieState.movieDetails).toEqual(undefined);
  });

  it('should set moviePhase to LOADING on loadMoviesAction', () => {
    const initialStateMovieState: MovieDetailsState = initialState;
    const action = loadMovieAction({ id: 333 });
    const state = fromMovie.reducer(initialStateMovieState, action);

    expect(state.moviePhase).toEqual(MovieDetailsStatePhaseEnum.LOADING);
    expect(state.error).toEqual('');
    expect(state.movieDetails).toEqual(undefined);
  });

  it('should set moviePhase to LOADING_ERROR and update error on loadMovieErrorAction', () => {
    const initialStateMovieState: MovieDetailsState = initialState;
    const action = loadMovieErrorAction({ error: 'bad bad bad' });
    const state = fromMovie.reducer(initialStateMovieState, action);

    expect(state.moviePhase).toEqual(MovieDetailsStatePhaseEnum.LOADING_ERROR);
    expect(state.error).toEqual('bad bad bad');
    expect(state.movieDetails).toBeFalsy();
  });

  it('should set moviePhase to LOADING_SUCCESSFUL and update  movieDetails on loadMovieSuccessAction', () => {
    const response: DetailsMovieResponse = new DetailsMovieResponse();

    const initialStateMoviesState: MovieDetailsState = initialState;
    const action = loadMovieSuccessAction({ response });
    const state = fromMovie.reducer(initialStateMoviesState, action);

    expect(state.moviePhase).toEqual(MovieDetailsStatePhaseEnum.LOADING_SUCCESSFUL);
    expect(state.error).toEqual('');
    expect(state.movieDetails).not.toBeFalsy();
  });
});
