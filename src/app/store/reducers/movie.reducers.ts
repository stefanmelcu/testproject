import {
  Action,
  createReducer,
  on,
} from '@ngrx/store';
import * as MoviesActions from '../actions/movies.actions';
import DetailsMovieResponse from '../../models/movies/details-movie-response.model';

export enum MovieDetailsStatePhaseEnum {
  'INIT',
  'LOADING',
  'LOADING_SUCCESSFUL',
  'LOADING_ERROR',
}

export interface MovieDetailsState {
  moviePhase: MovieDetailsStatePhaseEnum;
  movieDetails: DetailsMovieResponse | undefined;
  error: string;
}

export const initialState: MovieDetailsState = {
  moviePhase: MovieDetailsStatePhaseEnum.INIT,
  movieDetails: undefined,
  error: ''
};

const moviesReducer = createReducer(
  initialState,
  on(MoviesActions.loadMovieAction, state => ({
    ...state,
    moviePhase: MovieDetailsStatePhaseEnum.LOADING,
  })),
  on(MoviesActions.loadMovieSuccessAction, (state, action) => ({
    ...state,
    moviePhase: MovieDetailsStatePhaseEnum.LOADING_SUCCESSFUL,
    movieDetails: action.response,
    error: ''
  })),
  on(MoviesActions.loadMovieErrorAction, (state, action) => ({
    ...state,
    moviePhase: MovieDetailsStatePhaseEnum.LOADING_ERROR,
    movieDetails: undefined,
    error: action.error
  })),
);

export function reducer(state: MovieDetailsState | undefined, action: Action): MovieDetailsState {
  return moviesReducer(state, action);
}
