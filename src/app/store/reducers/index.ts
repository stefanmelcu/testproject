import {ActionReducerMap, MetaReducer} from '@ngrx/store';
import {environment} from '../../../environments/environment';
import * as fromMovies from './movies.reducers';
import * as fromMovie from './movie.reducers';
import {MoviesState} from './movies.reducers';
import {debug} from './meta.reducers';
import {storeFreeze} from 'ngrx-store-freeze';
import {MovieDetailsState} from './movie.reducers';

export const metaReducers: MetaReducer[] = !environment.production ? [debug, storeFreeze] : [];

export const moviesListResponseKey = 'moviesListResponse';
export const movieDetailsResponseKey = 'movieDetailsResponse';

export interface RootStoreState {
  [moviesListResponseKey]: MoviesState;
  [movieDetailsResponseKey]: MovieDetailsState;
}

export const reducers: ActionReducerMap<RootStoreState> = {
  moviesListResponse: fromMovies.reducer,
  movieDetailsResponse: fromMovie.reducer
};
