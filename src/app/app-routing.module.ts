import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MoviesPageComponent} from './components/movies-page/movies-page.component';
import {CONFIG} from '../config/config';
import {MovieDetailPageComponent} from './components/movie-detail-page/movie-detail-page.component';
import {MovieCheckGuard} from './guards/movie-check.guard';
import {NotFoundPageComponent} from './components/not-found-page/not-found-page.component';

const routes: Routes = [{
    path: CONFIG.ROUTES.HOME.SLUG,
    component: MoviesPageComponent,
    data: {
      title: 'Discover the best movies',
    },
  }, {
    path: CONFIG.ROUTES.MOVIE.SLUG,
    component: MovieDetailPageComponent,
    canActivate: [MovieCheckGuard],
    data: {
      title: 'Discover the best movies',
    },
  },
  {
    path: CONFIG.ROUTES.NOT_FOUND.SLUG,
    component: NotFoundPageComponent
  },
  {
    path: '**',
    redirectTo: CONFIG.ROUTES.NOT_FOUND.SLUG
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
