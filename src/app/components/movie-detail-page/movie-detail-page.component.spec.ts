import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MovieDetailPageComponent } from './movie-detail-page.component';
import {movieDetailsResponseKey, moviesListResponseKey, RootStoreState} from '../../store/reducers';
import {MoviesStatePhaseEnum} from '../../store/reducers/movies.reducers';
import {deserialize} from '../../../utils/deserializer';
import {discoverMovieResponseMock} from '../../models/movies/discover-movie-response.model.mock';
import DiscoverMovieResponse from '../../models/movies/discover-movie-response.model';
import {MovieDetailsStatePhaseEnum} from '../../store/reducers/movie.reducers';
import {detailsMovieResponseMock} from '../../models/movies/details-movie-response.model.mock';
import DetailsMovieResponse from '../../models/movies/details-movie-response.model';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {MoviesDBService} from '../../services/movies-db.service';
import {selectMovieDetailsPhase} from '../../store/selectors/movie.selectors';
import {HttpClientTestingModule} from '@angular/common/http/testing';


describe('MovieDetailPageComponent', () => {
  let component: MovieDetailPageComponent;
  let fixture: ComponentFixture<MovieDetailPageComponent>;
  let store: MockStore;
  let service: MoviesDBService;

  const fakeRootState: RootStoreState = {
    [moviesListResponseKey]: {
      moviesPhase: MoviesStatePhaseEnum.INIT,
      moviesListResponse: deserialize(
        discoverMovieResponseMock,
        DiscoverMovieResponse,
        { debug: false }
      ),
      error: 'good'
    },
    [movieDetailsResponseKey]: {
      moviePhase: MovieDetailsStatePhaseEnum.INIT,
      movieDetails: deserialize(
        detailsMovieResponseMock,
        DetailsMovieResponse,
        { debug: false }
      ),
      error: 'baaaad'
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovieDetailPageComponent ],
      providers: [
        provideMockStore({ initialState: fakeRootState }),
      ],
      imports: [
        HttpClientTestingModule
      ]
    }).compileComponents();

    service = TestBed.inject(MoviesDBService);
    store = TestBed.inject(MockStore);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieDetailPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`detailLoadingSpinner should only be present on the page if the page is indeed loading data`, () => {
    const detailLoadingSpinner = fixture.nativeElement.querySelector('[data-test="detailLoadingSpinner"]');

    const mockselectMovieDetailsPhase = store.overrideSelector(
      selectMovieDetailsPhase,
      MovieDetailsStatePhaseEnum.INIT
    );
    fixture.detectChanges();

    expect(detailLoadingSpinner).toBeFalsy();

    mockselectMovieDetailsPhase.setResult(MovieDetailsStatePhaseEnum.LOADING);
    store.refreshState();
    fixture.detectChanges();

    expect(fixture.nativeElement.querySelector('[data-test="detailLoadingSpinner"]')).toBeTruthy();
  });

  it(`errorDetailsAlert should only be present on the page if there is indeed a error`, () => {
    const alert = fixture.nativeElement.querySelector('[data-test="errorDetailsAlert"]');

    const mockselectMoviesListPhase = store.overrideSelector(
      selectMovieDetailsPhase,
      MovieDetailsStatePhaseEnum.INIT
    );
    fixture.detectChanges();

    expect(alert).toBeFalsy();

    mockselectMoviesListPhase.setResult(MovieDetailsStatePhaseEnum.LOADING_ERROR);
    store.refreshState();
    fixture.detectChanges();

    expect(fixture.nativeElement.querySelector('[data-test="errorDetailsAlert"]')).toBeTruthy();
    expect(fixture.nativeElement.querySelector('[data-test="errorDetailsAlertText"]').textContent).toContain('baaaad');
  });

  it(`mainDetailsPage should only be present on the page if the load was successful`, () => {
    const mainDetailsPage = fixture.nativeElement.querySelector('[data-test="mainDetailsPage"]');

    const mockselectMoviesListPhase = store.overrideSelector(
      selectMovieDetailsPhase,
      MovieDetailsStatePhaseEnum.LOADING_SUCCESSFUL
    );
    fixture.detectChanges();

    expect(mainDetailsPage).toBeFalsy();

    mockselectMoviesListPhase.setResult(MovieDetailsStatePhaseEnum.LOADING_SUCCESSFUL);
    store.refreshState();
    fixture.detectChanges();

    expect(fixture.nativeElement.querySelector('[data-test="mainDetailsPage"]')).toBeTruthy();
  });

  // INFO I've could have done a lot more tests here but for the sake
  // for this test there is nothing new I can do here that I have not already finished in the movie Card Component
  // Its basically just a pure component at this point.
});
