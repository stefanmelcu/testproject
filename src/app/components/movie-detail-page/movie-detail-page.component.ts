import { Component, OnInit } from '@angular/core';
import {select, Store} from '@ngrx/store';
import {tap} from 'rxjs/operators';
import {RootStoreState} from '../../store/reducers';
import {
  selectMovieDetails,
  selectMovieDetailsError,
  selectMovieDetailsPhase,
} from '../../store/selectors/movie.selectors';
import DetailsMovieResponse from '../../models/movies/details-movie-response.model';
import {ImageAPISizesEnum} from '../../models/movies';
import {Observable} from 'rxjs';
import {MovieDetailsStatePhaseEnum} from '../../store/reducers/movie.reducers';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-movie-detail-page',
  templateUrl: './movie-detail-page.component.html',
  styleUrls: ['./movie-detail-page.component.scss']
})
export class MovieDetailPageComponent implements OnInit {
  public  movieDetails: DetailsMovieResponse | undefined;
  public error: Observable<string>;
  public detailsPhase: Observable<MovieDetailsStatePhaseEnum>;

  public ImageAPISizesEnum = ImageAPISizesEnum;
  public MovieDetailsStatePhaseEnum = MovieDetailsStatePhaseEnum;

  constructor(
    private store: Store<RootStoreState>,
  ) {

    this.detailsPhase = this.store.pipe(
      select(selectMovieDetailsPhase),
      tap(data => environment.enableDebug && console.log(data))
    );

    this.store.pipe(
      select(selectMovieDetails),
      tap(data => environment.enableDebug && console.log(data))
    ).subscribe(value => {
      this.movieDetails = value;
    });

    this.error = this.store.pipe(
      select(selectMovieDetailsError),
      tap(data => environment.enableDebug && console.log(data))
    );

  }

  ngOnInit(): void {
  }

}
