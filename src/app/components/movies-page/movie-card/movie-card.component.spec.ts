import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MovieCardComponent } from './movie-card.component';
import {deserialize} from '../../../../utils/deserializer';
import {MoviesState, MoviesStatePhaseEnum} from '../../../store/reducers/movies.reducers';
import {discoverMovieResponseMock} from '../../../models/movies/discover-movie-response.model.mock';
import DiscoverMovieResponse from '../../../models/movies/discover-movie-response.model';
import {RouterTestingModule} from '@angular/router/testing';

describe('MovieCardComponent', () => {
  let component: MovieCardComponent;
  let fixture: ComponentFixture<MovieCardComponent>;
  let moviesList: MoviesState;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovieCardComponent ],
      imports: [ RouterTestingModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieCardComponent);
    component = fixture.componentInstance;
    moviesList = {
      moviesPhase: MoviesStatePhaseEnum.INIT,
      moviesListResponse: deserialize(
        discoverMovieResponseMock,
        DiscoverMovieResponse,
        {debug: false}
      ),
      error: 'goo'
    };
    component.movie = (moviesList.moviesListResponse as DiscoverMovieResponse).results[0];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`backdropImage should exist on the page only when a image is really available`, () => {
    if (component?.movie) {
      component.movie.backdropPath = 'aaaaa';
    }
    fixture.detectChanges();

    expect(fixture.nativeElement.querySelector('[data-test="backdropImage"]')).toBeTruthy();
  });

  it(`backdropImage should not exist on the page only when a image is not available`, () => {
    if (component?.movie) {
      component.movie.backdropPath = undefined;
    }
    fixture.detectChanges();

    expect(fixture.nativeElement.querySelector('[data-test="backdropImage"]')).toBeFalsy();
  });

  it(`movieTitle should exist on the page only when a title is available`, () => {
    if (component?.movie) {
      component.movie.title = 'whaaaat';
    }
    fixture.detectChanges();

    expect(fixture.nativeElement.querySelector('[data-test="movieTitle"]')).toBeTruthy();
    expect(fixture.nativeElement.querySelector('[data-test="movieTitle"]').textContent).toBe('whaaaat');
  });

  it(`overviewMovie should exist on the page only when a overview is available`, () => {
    if (component?.movie) {
      component.movie.overview = 'overview text';
    }
    fixture.detectChanges();

    expect(fixture.nativeElement.querySelector('[data-test="overviewMovie"]')).toBeTruthy();
    expect(fixture.nativeElement.querySelector('[data-test="overviewMovie"]').textContent).toBe('overview text');
  });

  it(`routerLinkToDetails should exist on the page aways`, () => {
    expect(fixture.nativeElement.querySelector('[data-test="routerLinkToDetails"]')).toBeTruthy();
  });

  it(`routerLinkToDetails should have a link to the details page`, () => {
    if (component?.movie) {
      component.movie.id = 666;
    }
    fixture.detectChanges();

    expect(fixture.nativeElement.querySelector('[data-test="routerLinkToDetails"]')
      .getAttribute('href')).toBe('/movie/666');
  });
});
