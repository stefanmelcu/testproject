import {Component, Input} from '@angular/core';
import MovieDetailsModel from '../../../models/movies/movie.model';

@Component({
  selector: 'app-movie-card',
  templateUrl: './movie-card.component.html',
  styleUrls: ['./movie-card.component.scss']
})
export class MovieCardComponent {
  @Input()
  public movie: MovieDetailsModel | undefined;

  constructor() { }
}
