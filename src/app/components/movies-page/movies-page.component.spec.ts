import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {MoviesPageComponent, SearchingByEnum} from './movies-page.component';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {movieDetailsResponseKey, moviesListResponseKey, RootStoreState} from '../../store/reducers';
import {MoviesStatePhaseEnum} from '../../store/reducers/movies.reducers';
import DiscoverMovieResponse from '../../models/movies/discover-movie-response.model';
import {MovieDetailsStatePhaseEnum} from '../../store/reducers/movie.reducers';
import DetailsMovieResponse from '../../models/movies/details-movie-response.model';
import {PaginationComponent} from './pagination/pagination.component';
import {MovieCardComponent} from './movie-card/movie-card.component';
import {MoviesDBService} from '../../services/movies-db.service';
import {FormBuilder, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {loadMoviesAction} from '../../store/actions/movies.actions';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {deserialize} from '../../../utils/deserializer';
import {discoverMovieResponseMock} from '../../models/movies/discover-movie-response.model.mock';
import {detailsMovieResponseMock} from '../../models/movies/details-movie-response.model.mock';
import MovieDetailsModel from '../../models/movies/movie.model';
import {DiscoverMovieSortByOptionsEnum} from '../../models/movies';
import {selectMoviesListPhase} from '../../store/selectors/movies.selectors';

describe('MoviesPageComponent', () => {
  let component: MoviesPageComponent;
  let fixture: ComponentFixture<MoviesPageComponent>;
  let store: MockStore;
  let service: MoviesDBService;
  let formBuilder: FormBuilder;
  const fakeRootState: RootStoreState = {
    [moviesListResponseKey]: {
      moviesPhase: MoviesStatePhaseEnum.INIT,
      moviesListResponse: deserialize(
        discoverMovieResponseMock,
        DiscoverMovieResponse,
        { debug: false }
      ),
      error: 'good'
    },
    [movieDetailsResponseKey]: {
      moviePhase: MovieDetailsStatePhaseEnum.INIT,
      movieDetails: deserialize(
        detailsMovieResponseMock,
        DetailsMovieResponse,
        { debug: false }
      ),
      error: 'baaaad'
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        MoviesPageComponent,
        MovieCardComponent,
        PaginationComponent
      ],
      imports: [
        ReactiveFormsModule,
        FormsModule,
        HttpClientTestingModule
      ],
      providers: [
        provideMockStore({ initialState: fakeRootState }),
        MoviesDBService,
      ]
    }).compileComponents();

    service = TestBed.inject(MoviesDBService);
    store = TestBed.inject(MockStore);
    formBuilder = TestBed.inject(FormBuilder);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoviesPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a public method getMovies', () => {
    expect(component.getMovies).toBeTruthy();
  });

  it('getMovies should dispatch movie fetch event', () => {
    const storeSpy = spyOn((component as any).store, 'dispatch');
    component.form.get('page')?.setValue(666);
    component.getMovies();

    expect(storeSpy).toHaveBeenCalledWith(loadMoviesAction({
      filters: {
        page: 666
      }
    }));
  });

  it('should have a pure getter moviesList', () => {
    expect(component.moviesList).toBeTruthy();
  });

  it('moviesList should return a list of MovieDetailsModel', () => {
    if (component?.selectMoviesList?.moviesListResponse) {
      component.selectMoviesList.moviesListResponse.results = [{ id: 666 } as MovieDetailsModel];
    }

    expect(component.moviesList[0].id).toBe(666);
  });

  it('moviesList should allays return a list', () => {
    if (component?.selectMoviesList?.moviesListResponse?.results) {
      component.selectMoviesList.moviesListResponse.results = undefined as unknown as any;
    }

    expect(component.moviesList).toEqual([]);
  });

  it('should have a static method isValidParamValue', () => {
    expect(MoviesPageComponent.isValidParamValue).toBeTruthy();
  });

  it('should return true if its not undefined, null or empty', () => {
    expect(MoviesPageComponent.isValidParamValue('aaa')).toBeTruthy();
    expect(MoviesPageComponent.isValidParamValue(33)).toBeTruthy();
    expect(MoviesPageComponent.isValidParamValue(['dddd'])).toBeTruthy();

    expect(MoviesPageComponent.isValidParamValue(undefined)).toBeFalsy();
    expect(MoviesPageComponent.isValidParamValue(null)).toBeFalsy();
    expect(MoviesPageComponent.isValidParamValue('')).toBeFalsy();
  });

  it('should have a method getHumanReadableValue', () => {
    expect(component.getHumanReadableValue).toBeTruthy();
  });

  it('getHumanReadableValue should format a movieDBapi value as a human readable format', () => {
    expect(component.getHumanReadableValue('DUMMY_ASC')).toBe('Dummy ↑ ascending');
  });

  it('should have a method onPageChange', () => {
    expect(component.onPageChange).toBeTruthy();
  });

  it('onPageChange should patch the form and call the getMovies method', () => {
    const componentSpy = spyOn(component, 'getMovies');
    const formSpy = spyOn(component.form, 'patchValue');

    component.onPageChange(444);

    expect(componentSpy).toHaveBeenCalled();
    expect(formSpy).toHaveBeenCalledWith({ page:  444 });
  });

  it('should have a method searchByPreset', () => {
    expect(component.onPageChange).toBeTruthy();
  });

  it('searchByPreset update for from and call the getMovies Method', () => {
    const componentSpy = spyOn(component, 'getMovies');
    const formSpy = spyOn(component.form, 'patchValue');

    component.searchByPreset(SearchingByEnum.MOST_POPULAR);

    expect(componentSpy).toHaveBeenCalled();
    expect(formSpy).toHaveBeenCalledWith({ sortBy: DiscoverMovieSortByOptionsEnum.POPULARITY_DESC });
  });

  it(`should moviesPageTitle text on the page at all times`, () => {
    expect(fixture.nativeElement.querySelector('[data-test="moviesPageTitle"]')).toBeTruthy();
    expect(fixture.nativeElement.querySelector('[data-test="moviesPageTitle"]').textContent).toContain(
      'Are you asking yourself'
    );
  });

  it(`searchByPresetMostPopularButton should always be present on the page`, () => {
    expect(fixture.nativeElement.querySelector('[data-test="searchByPresetMostPopularButton"]')).toBeTruthy();
  });

  it(`searchByPresetMostPopularButton should trigger a search and update the color of the button`, () => {
    const componentSpy = spyOn(component, 'getMovies');
    const button = fixture.nativeElement.querySelector('[data-test="searchByPresetMostPopularButton"]');
    button.click();

    fixture.detectChanges();

    expect(button.className).toContain('btn m-3 btn-sm btn-dark');
    expect(componentSpy).toHaveBeenCalled();
    expect(component.searchingBy).toBe(SearchingByEnum.MOST_POPULAR);
  });

  it(`searchByPresetWorstOf2020Button should always be present on the page`, () => {
    expect(fixture.nativeElement.querySelector('[data-test="searchByPresetWorstOf2020Button"]')).toBeTruthy();
  });

  it(`searchByPresetWorstOf2020Button should trigger a search and update the color of the button`, () => {
    const componentSpy = spyOn(component, 'getMovies');
    const button = fixture.nativeElement.querySelector('[data-test="searchByPresetWorstOf2020Button"]');
    button.click();

    fixture.detectChanges();

    expect(button.className).toContain('btn m-3 btn-sm btn-dark');
    expect(componentSpy).toHaveBeenCalled();
    expect(component.searchingBy).toBe(SearchingByEnum.WORST_2020);
  });

  it(`searchByHighestGoosingButton should always be present on the page`, () => {
    expect(fixture.nativeElement.querySelector('[data-test="searchByHighestGoosingButton"]')).toBeTruthy();
  });

  it(`searchByHighestGoosingButton should trigger a search and update the color of the button`, () => {
    const componentSpy = spyOn(component, 'getMovies');
    const button = fixture.nativeElement.querySelector('[data-test="searchByHighestGoosingButton"]');
    button.click();

    fixture.detectChanges();

    expect(button.className).toContain('btn m-3 btn-sm btn-dark');
    expect(componentSpy).toHaveBeenCalled();
    expect(component.searchingBy).toBe(SearchingByEnum.HIGHEST_GROSING);
  });


  it(`searchByOldestButton should always be present on the page`, () => {
    expect(fixture.nativeElement.querySelector('[data-test="searchByOldestButton"]')).toBeTruthy();
  });

  it(`searchByOldestButton should trigger a search and update the color of the button`, () => {
    const componentSpy = spyOn(component, 'getMovies');
    const button = fixture.nativeElement.querySelector('[data-test="searchByOldestButton"]');
    button.click();

    fixture.detectChanges();

    expect(button.className).toContain('btn m-3 btn-sm btn-dark');
    expect(componentSpy).toHaveBeenCalled();
    expect(component.searchingBy).toBe(SearchingByEnum.OLDEST);
  });

  it(`searchByNewestButton should always be present on the page`, () => {
    expect(fixture.nativeElement.querySelector('[data-test="searchByNewestButton"]')).toBeTruthy();
  });

  it(`searchByNewestButton should trigger a search and update the color of the button`, () => {
    const componentSpy = spyOn(component, 'getMovies');
    const button = fixture.nativeElement.querySelector('[data-test="searchByNewestButton"]');
    button.click();

    fixture.detectChanges();

    expect(button.className).toContain('btn m-3 btn-sm btn-dark');
    expect(componentSpy).toHaveBeenCalled();
    expect(component.searchingBy).toBe(SearchingByEnum.NEWEST);
  });


  it(`searchByVotedButton should always be present on the page`, () => {
    expect(fixture.nativeElement.querySelector('[data-test="searchByVotedButton"]')).toBeTruthy();
  });

  it(`searchByVotedButton should trigger a search and update the color of the button`, () => {
    const componentSpy = spyOn(component, 'getMovies');
    const button = fixture.nativeElement.querySelector('[data-test="searchByVotedButton"]');
    button.click();

    fixture.detectChanges();

    expect(button.className).toContain('btn m-3 btn-sm btn-dark');
    expect(componentSpy).toHaveBeenCalled();
    expect(component.searchingBy).toBe(SearchingByEnum.VOTED);
  });

  it(`searchByObscureButton should always be present on the page`, () => {
    expect(fixture.nativeElement.querySelector('[data-test="searchByObscureButton"]')).toBeTruthy();
  });

  it(`searchByObscureButton should trigger a search and update the color of the button`, () => {
    const componentSpy = spyOn(component, 'getMovies');
    const button = fixture.nativeElement.querySelector('[data-test="searchByObscureButton"]');
    button.click();

    fixture.detectChanges();

    expect(button.className).toContain('btn m-3 btn-sm btn-dark');
    expect(componentSpy).toHaveBeenCalled();
    expect(component.searchingBy).toBe(SearchingByEnum.OBSCURE);
  });

  it(`expandedAreaZone should always be present on the expandedAreaZone`, () => {
    expect(fixture.nativeElement.querySelector('[data-test="expandedAreaZone"]')).toBeTruthy();
  });

  it(`showFiltersButton should always be present on the page`, () => {
    expect(fixture.nativeElement.querySelector('[data-test="showFiltersButton"]')).toBeTruthy();
  });

  it(`showFiltersButton should show and hide the filters`, fakeAsync(() => {
    const button = fixture.nativeElement.querySelector('[data-test="showFiltersButton"]');
    const container = fixture.nativeElement.querySelector('[data-test="expandedAreaZone"]');

    button.click();
    fixture.detectChanges();
    tick(50);
    expect(component.isCollapsed).toBe(false);
    expect(container.className).toBe('container collapse show');

    tick(50);
    button.click();
    fixture.detectChanges();

    expect(container.className).toBe('container collapse');
  }));

  it(`sortByLabel should always be present on the page`, () => {
    expect(fixture.nativeElement.querySelector('[data-test="sortByLabel"]')).toBeTruthy();
  });

  it(`sortBySelect should always be present on the page`, () => {
    expect(fixture.nativeElement.querySelector('[data-test="sortBySelect"]')).toBeTruthy();
  });

  it(`sortBySelect should have a list of all 14 options for the field + the default disabled one`, () => {
    expect(fixture.nativeElement.querySelectorAll('[data-test="sortBySelect"] option').length).toBe(15);
  });


  it(`includeAdultLabel should always be present on the page`, () => {
    expect(fixture.nativeElement.querySelector('[data-test="includeAdultLabel"]')).toBeTruthy();
  });

  it(`includeAdultSelect should always be present on the page`, () => {
    expect(fixture.nativeElement.querySelector('[data-test="sortBySelect"]')).toBeTruthy();
  });

  it(`includeAdultSelect should have a list of all 14 options for the field + the default disabled one`, () => {
    expect(fixture.nativeElement.querySelectorAll('[data-test="includeAdultSelect"] option').length).toBe(3);
  });

  it(`yearLabel should always be present on the page`, () => {
    expect(fixture.nativeElement.querySelector('[data-test="yearLabel"]')).toBeTruthy();
  });

  it(`yearLabel should always be present on the page`, () => {
    expect(fixture.nativeElement.querySelector('[data-test="yearLabel"]')).toBeTruthy();
  });

  it(`yearSelect should have a list of all 14 options for the field + the default disabled one`, () => {
    expect(fixture.nativeElement.querySelectorAll('[data-test="yearSelect"] option').length).toBe(12);
  });

  it(`searchGenericButton should always exist on the page`, () => {
    expect(fixture.nativeElement.querySelector('[data-test="searchGenericButton"]')).toBeTruthy();
  });

  it(`searchGenericButton should trigger a search by the custom parameters and update the color of the button`, () => {
    const componentSpy = spyOn(component, 'getMovies');
    const button = fixture.nativeElement.querySelector('[data-test="searchGenericButton"]');
    button.click();

    fixture.detectChanges();

    expect(button.className).toContain('btn btn-dark');
    expect(componentSpy).toHaveBeenCalled();
    expect(component.searchingBy).toBe(SearchingByEnum.CUSTOM);
  });

  it(`searchGenericButton should always exist on the page`, () => {
    expect(fixture.nativeElement.querySelector('[data-test="searchGenericButton"]')).toBeTruthy();
  });

  it(`errorAlert should only be present on the page if there is indeed a error`, () => {
    const alert = fixture.nativeElement.querySelector('[data-test="errorAlert"]');

    const mockselectMoviesListPhase = store.overrideSelector(
      selectMoviesListPhase,
      MoviesStatePhaseEnum.INIT
    );
    fixture.detectChanges();

    expect(alert).toBeFalsy();

    mockselectMoviesListPhase.setResult(MoviesStatePhaseEnum.LOADING_ERROR);
    store.refreshState();
    fixture.detectChanges();

    expect(fixture.nativeElement.querySelector('[data-test="errorAlert"]')).toBeTruthy();
    expect(fixture.nativeElement.querySelector('[data-test="errorAlertText"]').textContent).toContain('good');
  });

  it(`loadingSpinner should only be present on the page is loading data`, () => {
    const spinner = fixture.nativeElement.querySelector('[data-test="loadingSpinner"]');

    const mockselectMoviesListPhase = store.overrideSelector(
      selectMoviesListPhase,
      MoviesStatePhaseEnum.INIT
    );
    fixture.detectChanges();

    expect(spinner).toBeFalsy();

    mockselectMoviesListPhase.setResult(MoviesStatePhaseEnum.LOADING);
    store.refreshState();
    fixture.detectChanges();

    expect(fixture.nativeElement.querySelector('[data-test="loadingSpinner"]')).toBeTruthy();
  });

  it(`moviesList should only be shown when we need it and if the load was successful`, () => {
    const moviesList = fixture.nativeElement.querySelector('[data-test="moviesList"]');

    const mockselectMoviesListPhase = store.overrideSelector(
      selectMoviesListPhase,
      MoviesStatePhaseEnum.INIT
    );
    fixture.detectChanges();

    expect(moviesList).toBeFalsy();

    mockselectMoviesListPhase.setResult(MoviesStatePhaseEnum.LOADING_SUCCESSFUL);
    component.selectMoviesList = fakeRootState[moviesListResponseKey];

    store.refreshState();
    fixture.detectChanges();

    expect(fixture.nativeElement.querySelector('[data-test="moviesList"]')).toBeTruthy();
  });

  it(`moviesPagination should only be shown when we need it and if the load was successful`, () => {
    const moviesPagination = fixture.nativeElement.querySelector('[data-test="moviesPagination"]');

    const mockselectMoviesListPhase = store.overrideSelector(
      selectMoviesListPhase,
      MoviesStatePhaseEnum.INIT
    );
    fixture.detectChanges();

    expect(moviesPagination).toBeFalsy();

    mockselectMoviesListPhase.setResult(MoviesStatePhaseEnum.LOADING_SUCCESSFUL);
    component.selectMoviesList = fakeRootState[moviesListResponseKey];
    store.refreshState();
    fixture.detectChanges();

    expect(fixture.nativeElement.querySelector('[data-test="moviesPagination"]')).toBeTruthy();
  });
});
