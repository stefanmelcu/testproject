import { Component, OnInit } from '@angular/core';
import {select, Store} from '@ngrx/store';
import {RootStoreState} from '../../store/reducers';
import {loadMoviesAction} from '../../store/actions/movies.actions';
import {selectMoviesListPhase, selectMoviesList, selectMoviesListError} from '../../store/selectors/movies.selectors';
import {tap} from 'rxjs/operators';
import {MoviesState, MoviesStatePhaseEnum} from '../../store/reducers/movies.reducers';
import {Observable} from 'rxjs';
import {CONFIG} from '../../../config/config';
import {DiscoverMovieIncludeAdultOptionsEnum, DiscoverMovieSortByOptionsEnum, MoviesFilter} from '../../models/movies';
import {MoviesDBService} from '../../services/movies-db.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import MovieDetailsModel from '../../models/movies/movie.model';
import {environment} from '../../../environments/environment';

// INFO this is only really used here no need to move it to a global file
export enum SearchingByEnum {
  NO_SEARCH,
  CUSTOM,
  MOST_POPULAR,
  WORST_2020,
  HIGHEST_GROSING,
  OLDEST,
  NEWEST,
  VOTED,
  OBSCURE
}

@Component({
  selector: 'app-movies-page',
  templateUrl: './movies-page.component.html',
  styleUrls: ['./movies-page.component.scss']
})
export class MoviesPageComponent implements OnInit {
  public listPhase: Observable<MoviesStatePhaseEnum>;
  public selectMoviesList: MoviesState | undefined;
  public isCollapsed: boolean;
  public searchingBy: SearchingByEnum;
  public form: FormGroup;
  public error: Observable<string>;

  public DiscoverMovieSortByOptionsEnum = DiscoverMovieSortByOptionsEnum;
  public DiscoverMovieIncludeAdultOptionsEnum = DiscoverMovieIncludeAdultOptionsEnum;

  public SearchingByEnum = SearchingByEnum;
  public MoviesStatePhaseEnum = MoviesStatePhaseEnum;

  public static isValidParamValue(value: any): boolean {
    return value !== undefined && value !== null && value;
  }

  // INFO take a api value and change it to look human readable.
  public getHumanReadableValue(key: string): string {
    const value: string = key
      .split('_')
      .join(' ')
      .split('DESC')
      .join('↓ Descending')
      .split('ASC')
      .join('↑ Ascending')
      .toLowerCase();

    return value.charAt(0).toUpperCase() + value.slice(1);
  }


  public get moviesList(): MovieDetailsModel[] {
    return this.selectMoviesList?.moviesListResponse?.results || [];
  }

  constructor(
    private store: Store<RootStoreState>,
    private service: MoviesDBService,
    private formBuilder: FormBuilder
  ) {
    this.searchingBy = SearchingByEnum.NO_SEARCH;
    this.isCollapsed = true;

    this.error = this.store.pipe(
      select(selectMoviesListError),
      tap(data => environment.enableDebug && console.log(data))
    );

    this.listPhase = this.store.pipe(
      select(selectMoviesListPhase),
      tap(data => environment.enableDebug && console.log(data))
    );

    this.store.pipe(
      select(selectMoviesList),
      tap(data => environment.enableDebug && console.log(data))
    ).subscribe(value => {
      this.selectMoviesList = value;
    });

    this.form = this.formBuilder.group({
      sortBy: '',
      includeAdult: '',
      page: 1,
      year: ''
    });
  }

  ngOnInit(): void {
  }

  getMovies(): void {
    const filters: MoviesFilter = {};

    if (MoviesPageComponent.isValidParamValue(this.form.get('sortBy')?.value)) {
      filters[`${CONFIG.APIS.MOVIE_API.PARAMS.SORT_BY}`] = this.form.get('sortBy')?.value;
    }

    if (MoviesPageComponent.isValidParamValue(this.form.get('page')?.value)) {
      filters[`${CONFIG.APIS.MOVIE_API.PARAMS.PAGE_KEY}`] = this.form.get('page')?.value;
    }

    if (MoviesPageComponent.isValidParamValue(this.form.get('includeAdult')?.value)) {
      filters[`${CONFIG.APIS.MOVIE_API.PARAMS.INCLUDE_ADULT}`] = this.form.get('includeAdult')?.value;
    }

    if (MoviesPageComponent.isValidParamValue(this.form.get('year')?.value)) {
      filters[`${CONFIG.APIS.MOVIE_API.PARAMS.YEAR}`] = this.form.get('year')?.value;
    }

    this.store.dispatch(loadMoviesAction({ filters }));
  }

  onPageChange(page: number): void {
    this.form.patchValue({ page });
    this.getMovies();
  }

  searchByPreset(by: SearchingByEnum): void {
    switch (by) {
      case SearchingByEnum.MOST_POPULAR:
        this.form.reset();
        this.form.patchValue({
          sortBy: DiscoverMovieSortByOptionsEnum.POPULARITY_DESC,
        });
        this.form.markAsDirty();
        break;
      case SearchingByEnum.WORST_2020:
        this.form.reset();
        this.form.patchValue({
          sortBy: DiscoverMovieSortByOptionsEnum.POPULARITY_ASC,
          year: 2020
        });
        this.form.markAsDirty();
        break;
      case SearchingByEnum.HIGHEST_GROSING:
        this.form.reset();
        this.form.patchValue({
          sortBy: DiscoverMovieSortByOptionsEnum.REVENUE_DESC
        });
        this.form.markAsDirty();
        break;
      case SearchingByEnum.OLDEST:
        this.form.reset();
        this.form.patchValue({
          sortBy: DiscoverMovieSortByOptionsEnum.PRIMARY_RELEASE_DATE_ASC
        });
        this.form.markAsDirty();
        break;
      case SearchingByEnum.NEWEST:
        this.form.reset();
        this.form.patchValue({
          sortBy: DiscoverMovieSortByOptionsEnum.PRIMARY_RELEASE_DATE_DESC
        });
        this.form.markAsDirty();
        break;
      case SearchingByEnum.VOTED:
        this.form.reset();
        this.form.patchValue({
          sortBy: DiscoverMovieSortByOptionsEnum.VOTE_COUNT_DESC
        });
        this.form.markAsDirty();
        break;
      case SearchingByEnum.OBSCURE:
        this.form.reset();
        this.form.patchValue({
          sortBy: DiscoverMovieSortByOptionsEnum.VOTE_COUNT_ASC
        });
        this.form.markAsDirty();
        break;
      case SearchingByEnum.CUSTOM:
      default:
    }

    this.getMovies();
    this.searchingBy = by;
  }
}
