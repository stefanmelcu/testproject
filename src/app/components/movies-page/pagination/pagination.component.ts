import { Component, Output, EventEmitter, Input } from '@angular/core';
import {MoviesState} from '../../../store/reducers/movies.reducers';


// INFO this is a close as you get to a pure component not connected to Redux
@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent {
  @Input()
  public moviesList: MoviesState | undefined;

  @Output()
  public pageChange = new EventEmitter<number>();

  public get currentPage(): number {
    return this.moviesList?.moviesListResponse?.page || 1;
  }

  public get totalPages(): number {
    return this.moviesList?.moviesListResponse?.totalPages || 0;
  }

  constructor() { }

}
