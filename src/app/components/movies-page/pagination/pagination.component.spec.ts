import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginationComponent } from './pagination.component';
import {MoviesState, MoviesStatePhaseEnum} from '../../../store/reducers/movies.reducers';
import {deserialize} from '../../../../utils/deserializer';
import {discoverMovieResponseMock} from '../../../models/movies/discover-movie-response.model.mock';
import DiscoverMovieResponse from '../../../models/movies/discover-movie-response.model';

describe('PaginationComponent', () => {
  let component: PaginationComponent;
  let fixture: ComponentFixture<PaginationComponent>;
  let moviesList: MoviesState;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaginationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginationComponent);
    component = fixture.componentInstance;
    moviesList = {
      moviesPhase: MoviesStatePhaseEnum.INIT,
      moviesListResponse: deserialize(
        discoverMovieResponseMock,
        DiscoverMovieResponse,
        { debug: false }
      ),
      error: 'good'
    };
    component.moviesList = moviesList;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`PreviousButton should always exist on the page`, () => {
    expect(fixture.nativeElement.querySelector('[data-test="PreviousButton"]')).toBeTruthy();
  });

  it(`NextButton should always exist on the page`, () => {
    expect(fixture.nativeElement.querySelector('[data-test="NextButton"]')).toBeTruthy();
  });

  it('should have a valid currentPage setter', () => {
    expect(component.currentPage).toBeTruthy();
  });

  it('totalPages should always return a number', () => {
    if (component?.moviesList?.moviesListResponse?.page) {
      component.moviesList.moviesListResponse.totalPages = 0;
    }
    fixture.detectChanges();

    expect(component.totalPages).toEqual(0);
  });

  it('currentPage should always return a number', () => {
    if (component?.moviesList?.moviesListResponse?.page) {
      component.moviesList.moviesListResponse.page = 1;
    }
    fixture.detectChanges();

    expect(component.currentPage).toEqual(1);
  });


  it(`PreviousButton should be disabled if the current page is lower then 2`, () => {
    if (component?.moviesList?.moviesListResponse) {
      component.moviesList.moviesListResponse.page = 1;
    }
    fixture.detectChanges();

    expect(fixture.nativeElement.querySelector('[data-test="PreviousButton"]').disabled).toBeTruthy();
  });

  it(`NextButton should be disabled if the current page if the same as the last page`, () => {
    if (component?.moviesList?.moviesListResponse) {
      component.moviesList.moviesListResponse.page = component.moviesList.moviesListResponse.totalPages;
    }
    fixture.detectChanges();

    expect(fixture.nativeElement.querySelector('[data-test="NextButton"]').disabled).toBeTruthy();
  });
});
