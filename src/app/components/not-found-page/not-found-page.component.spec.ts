import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotFoundPageComponent } from './not-found-page.component';

describe('NotFoundPageComponent', () => {
  let component: NotFoundPageComponent;
  let fixture: ComponentFixture<NotFoundPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotFoundPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotFoundPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should display the 404 text on the page at all times`, () => {
    expect(fixture.nativeElement.querySelector('[data-test="404Text"]')).toBeTruthy();
    expect(fixture.nativeElement.querySelector('[data-test="404Text"]').textContent).toContain(
      'Sorry Page not Found :(. You should head to the home page'
    );
  });
});
