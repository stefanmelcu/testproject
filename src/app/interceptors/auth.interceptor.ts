import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import {appendQueryStringToUrl, toQueryString} from '../../utils/querry-string';
import {CONFIG} from '../../config/config';


@Injectable({
  providedIn: 'root'
})
export class AuthInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    // INFO we create a new request and add the api key to it
    // (since the HttpRequest props are imitable and cant be changed directly)
    const httpRequest = new HttpRequest(
      request.method as any,
      appendQueryStringToUrl(
        request.url,
        toQueryString({ [`${CONFIG.APIS.MOVIE_API.PARAMS.API_KEY}`]: CONFIG.KEYS.MOVIE_DB_KEY })
      )
    );
    request = Object.assign(request, httpRequest);

    return next.handle(request);
  }
}
