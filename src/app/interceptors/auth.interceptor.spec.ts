import { TestBed } from '@angular/core/testing';
import { AuthInterceptor } from './auth.interceptor';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {MoviesDBService} from '../services/movies-db.service';
import {CONFIG} from '../../config/config';
import {HTTP_INTERCEPTORS} from '@angular/common/http';

describe('AuthInterceptor', () => {
  let interceptor: AuthInterceptor;
  let httpTestingController: HttpTestingController;
  let service: MoviesDBService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        MoviesDBService,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: AuthInterceptor,
          multi: true,
        },
      ]
    });

    service = TestBed.inject(MoviesDBService);
    interceptor = TestBed.inject(AuthInterceptor);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(interceptor).toBeTruthy();
  });

  it('should add an Authorization key param to the api call', () => {
    service.getMovie(33).subscribe();

    const mockUrl = `${CONFIG.APIS.MOVIE_API.BASE_URL}${CONFIG.APIS.MOVIE_API.PATHS.DETAILS}33`;

    const httpRequest = httpTestingController.expectOne(`${mockUrl}?${CONFIG.APIS.MOVIE_API.PARAMS.API_KEY}=${CONFIG.KEYS.MOVIE_DB_KEY}`);

    expect(httpRequest.request.url).toEqual(`${mockUrl}?${CONFIG.APIS.MOVIE_API.PARAMS.API_KEY}=${CONFIG.KEYS.MOVIE_DB_KEY}`);

    httpRequest.flush({});
  });
});


