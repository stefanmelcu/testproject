import { TestBed } from '@angular/core/testing';
import { MoviesDBService } from './movies-db.service';
import {HttpClientTestingModule, HttpTestingController, TestRequest} from '@angular/common/http/testing';
import {appendQueryStringToUrl, toQueryString} from '../../utils/querry-string';
import {CONFIG} from '../../config/config';
import {MoviesFilter} from '../models/movies';

describe('MoviesDBService', () => {
  let service: MoviesDBService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ MoviesDBService ],
      imports: [ HttpClientTestingModule ]
    });
    service = TestBed.inject(MoviesDBService);

    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(MoviesDBService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should correctly make call the out and retrieve the movies list.', () => {
    const filters: MoviesFilter = { test: 'param', test2: 'param' };
    const mockUrl =  appendQueryStringToUrl(
      `${CONFIG.APIS.MOVIE_API.BASE_URL}${CONFIG.APIS.MOVIE_API.PATHS.DISCOVER}`,
      toQueryString(filters)
    );

    service.getMovies(filters).subscribe();

    const reqest: TestRequest = httpTestingController.expectOne(mockUrl);
    expect(reqest.request.method).toBe('GET');
    reqest.flush({});
  });

  it('should correctly make call the out rerun a error if the request fails .', () => {
    const filters: MoviesFilter = { test: 'param', test2: 'param' };
    const mockUrl =  appendQueryStringToUrl(
      `${CONFIG.APIS.MOVIE_API.BASE_URL}${CONFIG.APIS.MOVIE_API.PATHS.DISCOVER}`,
      toQueryString(filters)
    );

    service.getMovies(filters).subscribe(() => {}, error => {
      expect(error).toBe(`Error: a big big error!`);
    });

    const reqest: TestRequest = httpTestingController.expectOne(mockUrl);
    expect(reqest.request.method).toBe('GET');

    reqest.error(new ErrorEvent('GenericErrorType', {
      error : new Error('GenericError'),
      message : 'a big big error!',
    }));
  });

  it('should correctly make call the out rerun a error if the request fails with a non-standard status (other then 404 and 401).', () => {
    const filters: MoviesFilter = { test: 'param', test2: 'param' };
    const mockUrl =  appendQueryStringToUrl(
      `${CONFIG.APIS.MOVIE_API.BASE_URL}${CONFIG.APIS.MOVIE_API.PATHS.DISCOVER}`,
      toQueryString(filters)
    );

    service.getMovies(filters).subscribe(() => {}, error => {
      expect(error).toBe(`Oops something went horribly wrong, please contact the administrator`);
    });

    const reqest: TestRequest = httpTestingController.expectOne(mockUrl);
    expect(reqest.request.method).toBe('GET');

    reqest.error({} as ErrorEvent, { status: 500 });
  });


  it('should correctly make call the out rerun a error if the request fails with a standard status 401.', () => {
    const filters: MoviesFilter = { test: 'param', test2: 'param' };
    const mockUrl =  appendQueryStringToUrl(
      `${CONFIG.APIS.MOVIE_API.BASE_URL}${CONFIG.APIS.MOVIE_API.PATHS.DISCOVER}`,
      toQueryString(filters)
    );

    service.getMovies(filters).subscribe(() => {}, error => {
      expect(error).toBe(`Error: bad 401`);
    });

    const reqest: TestRequest = httpTestingController.expectOne(mockUrl);
    expect(reqest.request.method).toBe('GET');

    reqest.error({
      status_message: 'bad 401',
    } as unknown as ErrorEvent, { status: 401 });
  });

  it('should correctly make call the out rerun a error if the request fails with a standard status 404.', () => {
    const filters: MoviesFilter = { test: 'param', test2: 'param' };
    const mockUrl =  appendQueryStringToUrl(
      `${CONFIG.APIS.MOVIE_API.BASE_URL}${CONFIG.APIS.MOVIE_API.PATHS.DISCOVER}`,
      toQueryString(filters)
    );

    service.getMovies(filters).subscribe(() => {}, error => {
      expect(error).toBe(`Error: bad 404`);
    });

    const reqest: TestRequest = httpTestingController.expectOne(mockUrl);
    expect(reqest.request.method).toBe('GET');

    reqest.error({
      status_message: 'bad 404',
    } as unknown as ErrorEvent, { status: 404 });
  });



  it('should correctly make call the out and retrieve the movies details.', () => {
    const mockUrl =  `${CONFIG.APIS.MOVIE_API.BASE_URL}${CONFIG.APIS.MOVIE_API.PATHS.DETAILS}33`;

    service.getMovie(33).subscribe();

    const reqest: TestRequest = httpTestingController.expectOne(mockUrl);
    expect(reqest.request.method).toBe('GET');
    reqest.flush({});
  });

  it('should correctly make call the out rerun a error if the request fails .', () => {
    const mockUrl =  `${CONFIG.APIS.MOVIE_API.BASE_URL}${CONFIG.APIS.MOVIE_API.PATHS.DETAILS}33`;

    service.getMovie(33).subscribe(() => {}, error => {
      expect(error).toBe(`Error: a big big error!`);
    });

    const reqest: TestRequest = httpTestingController.expectOne(mockUrl);
    expect(reqest.request.method).toBe('GET');

    reqest.error(new ErrorEvent('GenericErrorType', {
      error : new Error('GenericError'),
      message : 'a big big error!',
    }));
  });

  it('should correctly make call the out rerun a error if the request fails with a non-standard status (other then 404 and 401).', () => {
    const mockUrl =  `${CONFIG.APIS.MOVIE_API.BASE_URL}${CONFIG.APIS.MOVIE_API.PATHS.DETAILS}33`;

    service.getMovie(33).subscribe(() => {}, error => {
      expect(error).toBe(`Oops something went horribly wrong, please contact the administrator`);
    });

    const reqest: TestRequest = httpTestingController.expectOne(mockUrl);
    expect(reqest.request.method).toBe('GET');

    reqest.error({} as ErrorEvent, { status: 500 });
  });


  it('should correctly make call the out rerun a error if the request fails with a standard status 401.', () => {
    const mockUrl =  `${CONFIG.APIS.MOVIE_API.BASE_URL}${CONFIG.APIS.MOVIE_API.PATHS.DETAILS}33`;

    service.getMovie(33).subscribe(() => {}, error => {
      expect(error).toBe(`Error: bad 401`);
    });

    const reqest: TestRequest = httpTestingController.expectOne(mockUrl);
    expect(reqest.request.method).toBe('GET');

    reqest.error({
      status_message: 'bad 401',
    } as unknown as ErrorEvent, { status: 401 });
  });

  it('should correctly make call the out rerun a error if the request fails with a standard status 404.', () => {
    const mockUrl =  `${CONFIG.APIS.MOVIE_API.BASE_URL}${CONFIG.APIS.MOVIE_API.PATHS.DETAILS}33`;

    service.getMovie(33).subscribe(() => {}, error => {
      expect(error).toBe(`Error: bad 404`);
    });

    const reqest: TestRequest = httpTestingController.expectOne(mockUrl);
    expect(reqest.request.method).toBe('GET');

    reqest.error({
      status_message: 'bad 404',
    } as unknown as ErrorEvent, { status: 404 });
  });
});
