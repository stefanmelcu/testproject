import { Injectable } from '@angular/core';
import {CONFIG} from '../../config/config';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {appendQueryStringToUrl, toQueryString} from '../../utils/querry-string';
import {MoviesFilter} from '../models/movies';
import {catchError, map, tap} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';
import {deserialize} from '../../utils/deserializer';
import {environment} from '../../environments/environment';
import DiscoverMovieResponse from '../models/movies/discover-movie-response.model';
import DetailsMovieResponse from '../models/movies/details-movie-response.model';

@Injectable({
  providedIn: 'root'
})
export class MoviesDBService {

  constructor(private http: HttpClient) { }

  getMovies(filters: MoviesFilter): Observable<DiscoverMovieResponse> {
    return this.http.get<DiscoverMovieResponse>(
      appendQueryStringToUrl(`${CONFIG.APIS.MOVIE_API.BASE_URL}${CONFIG.APIS.MOVIE_API.PATHS.DISCOVER}`, toQueryString(filters)),
      { observe: 'response' }
    ).pipe(
      tap((response: HttpResponse<DiscoverMovieResponse >) => environment.enableDebug && console.log(response)),
      map(response => deserialize<DiscoverMovieResponse>(response.body, DiscoverMovieResponse, { debug: environment.enableDebug })),
      catchError(error => {
        // INFO Server-side error, handling every case we have documented in the official api
        // https://developers.themoviedb.org/3/discover/movie-discover
        switch (error.status) {
          case 401:
          case 404:
            return throwError( `Error: ${error.error.status_message}`);
          default:
            // INFO Client-side error, who knows what can break
            if (error.error instanceof ErrorEvent) {
              return throwError(`Error: ${error.error.message}`);
            }

            return throwError('Oops something went horribly wrong, please contact the administrator');
        }
      })
    );
  }

  getMovie(id: number): Observable<DetailsMovieResponse> {
    return this.http.get<DetailsMovieResponse>(
      `${CONFIG.APIS.MOVIE_API.BASE_URL}${CONFIG.APIS.MOVIE_API.PATHS.DETAILS}${id}`,
      { observe: 'response' }
    ).pipe(
      tap((response: HttpResponse<DetailsMovieResponse >) => environment.enableDebug && console.log(response)),
      map(response => deserialize<DetailsMovieResponse>(response.body, DetailsMovieResponse, { debug: environment.enableDebug })),
      catchError(error => {
        // INFO Server-side error, handling every case we have documented in the official api
        switch (error.status) {
          case 401:
          case 404:
            return throwError( `Error: ${error.error.status_message}`);
          default:
            // INFO Client-side error, who knows what can break
            if (error.error instanceof ErrorEvent) {
              return throwError(`Error: ${error.error.message}`);
            }

            return throwError('Oops something went horribly wrong, please contact the administrator');
        }
      })
    );
  }
}
