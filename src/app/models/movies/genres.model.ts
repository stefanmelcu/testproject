import { JsonObject, JsonProperty } from 'json2typescript';

@JsonObject('GenresModel')
export default class GenresModel {
  @JsonProperty('id', Number, true)
  public id: number | undefined = 1;

  @JsonProperty('name', String, true)
  public name: string | undefined  = '';

  [propName: string]: string | string[] | any;
}

// "genres": [
//   {
//     "id": 18,
//     "name": "Drama"
//   }
// ],
