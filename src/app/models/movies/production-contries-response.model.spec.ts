import {deserialize} from '../../../utils/deserializer';
import ProductionCountriesResponseModel from './production-contries-response.model';

describe('ProductionCountriesResponseModel', () => {
  const mockRowModel: any =   {
    iso_3166_1: 'US',
    name: 'United States of America'
  };
  let model: any;

  beforeEach(() => {
    model = undefined;
    model = deserialize(
      mockRowModel,
      ProductionCountriesResponseModel,
      { debug: false }
    );
  });

  it('should correctly deserialize the object', () => {
    expect(model instanceof ProductionCountriesResponseModel).toBeTruthy();
  });
});

