
import { JsonObject, JsonProperty } from 'json2typescript';
import {ImageAPISizesEnum} from './index';
import {CONFIG} from '../../../config/config';

@JsonObject('ProductionCompaniesResponseModel')
export default class ProductionCompaniesResponseModel {
  @JsonProperty('id', Number, true)
  public id: number | undefined = 1;

  @JsonProperty('logo_path', String, true)
  public logoPath: string | undefined  = '';

  @JsonProperty('name', String, true)
  public name: string | undefined  = '';

  @JsonProperty('origin_country', String, true)
  public originCountry: string | undefined  = '';

  public getLogoPath(size: ImageAPISizesEnum = ImageAPISizesEnum.W100): string {
    return this.logoPath ? `${CONFIG.APIS.MOVIE_API.IMAGE_RESOURCES_URL}/${size}${this.logoPath}` : '';
  }

  [propName: string]: string | string[] | any;
}
