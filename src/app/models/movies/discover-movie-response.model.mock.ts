export const discoverMovieResponseMock: any = {
  page: 1,
  total_results: 10000,
  total_pages: 500,
  results: [
    {
      popularity: 958.516,
      vote_count: 940,
      video: false,
      poster_path: '\/TnOeov4w0sTtV2gqICqIxVi74V.jpg',
      id: 605116,
      adult: false,
      backdrop_path: '\/qVygtf2vU15L2yKS4Ke44U4oMdD.jpg',
      original_language: 'en',
      original_title: 'Project Power',
      genre_ids: [
        28,
        80,
        878
      ],
      title: 'Project Power',
      vote_average: 6.7,
      overview: 'An ex-soldier, a teen and a cop collide in New Orleans as they hunt for the source behind a dangerous new pill that grants users temporary superpowers.',
      release_date: '2020-08-14'
    },
    {
      popularity: 854.483,
      vote_count: 100,
      video: false,
      poster_path: '\/uOw5JD8IlD546feZ6oxbIjvN66P.jpg',
      id: 718444,
      adult: false,
      backdrop_path: '\/x4UkhIQuHIJyeeOTdcbZ3t3gBSa.jpg',
      original_language: 'en',
      original_title: 'Rogue',
      genre_ids: [
        28
      ],
      title: 'Rogue',
      vote_average: 6,
      overview: 'Battle-hardened O’Hara leads a lively mercenary team of soldiers on a daring mission: rescue hostages from their captors in remote Africa. But as the mission goes awry and the team is stranded, O’Hara’s squad must face a bloody, brutal encounter with a gang of rebels.',
      release_date: '2020-08-20'
    }
  ]
};
