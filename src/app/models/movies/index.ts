// INFO all api values would be idealy stored in MAPS or ENUMS
// for easy and standard usage in the code
// (can even work stored in a CONFIG but it can become really blotted.
export enum DiscoverMovieSortByOptionsEnum {
  POPULARITY_DESC = 'popularity.desc',
  POPULARITY_ASC = 'popularity.asc',
  RELEASE_DATE_ASC = 'release_date.asc',
  RELEASE_DATE_DESC = 'release_date.desc',
  REVENUE_ASC = 'revenue.asc',
  REVENUE_DESC = 'revenue.desc',
  PRIMARY_RELEASE_DATE_ASC = 'primary_release_date.asc',
  PRIMARY_RELEASE_DATE_DESC = 'primary_release_date.desc',
  ORIGINAL_TITLE_ASC = 'original_title.asc',
  ORIGINAL_TITLE_DESC = 'original_title.desc',
  VOTE_AVERAGE_ASC = 'vote_average.asc',
  VOTE_AVERAGE_DESC = 'vote_average.desc',
  VOTE_COUNT_ASC = 'vote_count.asc',
  VOTE_COUNT_DESC = 'vote_count.desc'
}

export enum DiscoverMovieIncludeAdultOptionsEnum {
  YES = 'true',
  NO = 'false',
}

export enum ImageAPISizesEnum {
  W500 = 'w500',
  W300 = 'w300',
  W100 = 'w100'
}

export interface MoviesFilter {
  [propName: string]: string | number | any;
}
