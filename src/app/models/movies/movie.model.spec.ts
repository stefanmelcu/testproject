import {deserialize} from '../../../utils/deserializer';
import MovieDetailsModel from './movie.model';
import DetailsMovieResponse from './details-movie-response.model';
import {ImageAPISizesEnum} from './index';
import {CONFIG} from '../../../config/config';

describe('MovieDetailsModel', () => {
  const mockRowModel: any = {
      popularity: 438.63,
      vote_count: 841,
      video: false,
      poster_path: '\/TnOeov4w0sTtV2gqICqIxVi74V.jpg',
      id: 605116,
      adult: false,
      backdrop_path: '\/qVygtf2vU15L2yKS4Ke44U4oMdD.jpg',
      original_language: 'en',
      original_title: 'Project Power',
      genre_ids: [
        28,
        80,
        878
      ],
      title: 'Project Power',
      vote_average: 6.7,
      overview: 'An ex-soldier, a teen and a cop collide in New Orleans as they hunt for the source behind a dangerous new pill that grants users temporary superpowers.',
      release_date: '2020-08-14'
  };
  let model: any;

  beforeEach(() => {
    model = undefined;
    model = deserialize(
      mockRowModel,
      MovieDetailsModel,
      { debug: false }
    );
  });

  it('should correctly deserialize the object', () => {
    expect(model instanceof MovieDetailsModel).toBeTruthy();
  });

  it('should correctly get the backdrop', () => {
    expect((model as DetailsMovieResponse).getBackdrop(ImageAPISizesEnum.W300)).toBe(
      `${CONFIG.APIS.MOVIE_API.IMAGE_RESOURCES_URL}/w300${(model as DetailsMovieResponse).backdropPath}`
    );
  });

  it('should correctly get the poster', () => {
    expect((model as DetailsMovieResponse).getPoster(ImageAPISizesEnum.W100)).toBe(
      `${CONFIG.APIS.MOVIE_API.IMAGE_RESOURCES_URL}/w100${(model as DetailsMovieResponse).posterPath}`
    );
  });
});

