import DetailsMovieResponse from './details-movie-response.model';
import {deserialize} from '../../../utils/deserializer';
import {CONFIG} from '../../../config/config';
import {ImageAPISizesEnum} from './index';
import {detailsMovieResponseMock} from './details-movie-response.model.mock';

describe('DetailsMovieResponse', () => {
  const mockRowModel: any = detailsMovieResponseMock;
  let model: any;

  beforeEach(() => {
    model = undefined;
    model = deserialize(
      mockRowModel,
      DetailsMovieResponse,
      { debug: false }
    );
  });

  it('should correctly deserialize the object', () => {
    expect(model instanceof DetailsMovieResponse).toBeTruthy();
  });

  it('should correctly get the backdrop', () => {
    expect((model as DetailsMovieResponse).getBackdrop(ImageAPISizesEnum.W300)).toBe(
      `${CONFIG.APIS.MOVIE_API.IMAGE_RESOURCES_URL}/w300${(model as DetailsMovieResponse).backdropPath}`
    );
  });

  it('should correctly get the poster', () => {
    expect((model as DetailsMovieResponse).getPoster(ImageAPISizesEnum.W100)).toBe(
      `${CONFIG.APIS.MOVIE_API.IMAGE_RESOURCES_URL}/w100${(model as DetailsMovieResponse).posterPath}`
    );
  });
});

