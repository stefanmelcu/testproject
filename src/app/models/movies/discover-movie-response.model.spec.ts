import {deserialize} from '../../../utils/deserializer';
import DiscoverMovieResponse from './discover-movie-response.model';
import {discoverMovieResponseMock} from './discover-movie-response.model.mock';

describe('DiscoverMovieResponse', () => {
  const mockRowModel: any = discoverMovieResponseMock;
  let model: any;

  beforeEach(() => {
    model = undefined;
    model = deserialize(
      mockRowModel,
      DiscoverMovieResponse,
      { debug: false }
    );
  });

  it('should correctly deserialize the object', () => {
    expect(model instanceof DiscoverMovieResponse).toBeTruthy();
  });
});

