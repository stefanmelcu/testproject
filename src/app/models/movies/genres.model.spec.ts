import {deserialize} from '../../../utils/deserializer';
import GenresModel from './genres.model';

describe('GenresModel', () => {
  const mockRowModel: any =   {
    id: 18,
    name: 'Drama'
  };
  let model: any;

  beforeEach(() => {
    model = undefined;
    model = deserialize(
      mockRowModel,
      GenresModel,
      { debug: false }
    );
  });

  it('should correctly deserialize the object', () => {
    expect(model instanceof GenresModel).toBeTruthy();
  });
});

