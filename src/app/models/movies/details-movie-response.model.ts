
import { JsonObject, JsonProperty } from 'json2typescript';
import {ISODateConverter} from '../../convertors/iso-date.convertor';
import GenresModel from './genres.model';
import ProductionCompaniesResponseModel from './production-companies-response.model';
import ProductionCountriesResponseModel from './production-contries-response.model';
import SpokenLanguagesModel from './spoken-languages.model';
import {ImageAPISizesEnum} from './index';
import {CONFIG} from '../../../config/config';


@JsonObject('DetailsMovieResponse')
export default class DetailsMovieResponse {
  @JsonProperty('id', Number, true)
  public id: number | undefined = undefined;

  @JsonProperty('adult', Boolean, true)
  public adult = true;

  @JsonProperty('backdrop_path', String, true)
  public backdropPath: string | undefined = undefined;

  @JsonProperty('budget', Number, true)
  public budget: number | undefined = undefined;

  @JsonProperty('genres', [GenresModel], true)
  public genres: GenresModel[] = [];

  @JsonProperty('homepage', String, true)
  public homepage: string | undefined = '';

  @JsonProperty('imdb_id', String, true)
  public imdbId: string | undefined = '';

  @JsonProperty('original_language', String, true)
  public originalLanguage: string | undefined = '';

  @JsonProperty('original_title', String, true)
  public originalTitle: string | undefined = undefined;

  @JsonProperty('overview', String, true)
  public overview: string | undefined = undefined;

  @JsonProperty('popularity', Number, true)
  public popularity: number | undefined = undefined;

  @JsonProperty('poster_path', String, true)
  public posterPath: string | undefined = undefined;

  @JsonProperty('production_companies', [ProductionCompaniesResponseModel], true)
  public productionCompanies: ProductionCompaniesResponseModel[] = [];

  @JsonProperty('production_countries', [ProductionCountriesResponseModel], true)
  public productionCountries: ProductionCountriesResponseModel[] = [];

  @JsonProperty('release_date', ISODateConverter, true)
  public releaseDate: Date | undefined = undefined;

  @JsonProperty('revenue', Number, true)
  public revenue: number | undefined = undefined;

  @JsonProperty('runtime', Number, true)
  public runtime: number | undefined = undefined;

  @JsonProperty('spoken_languages', [SpokenLanguagesModel], true)
  public spokenLanguages: SpokenLanguagesModel[] = [];

  @JsonProperty('status', String, true)
  public status: string | undefined = undefined;

  @JsonProperty('tagline', String, true)
  public tagline: string | undefined = undefined;

  @JsonProperty('title', String, true)
  public title: string | undefined = undefined;

  @JsonProperty('video', Boolean, true)
  public video = false;

  @JsonProperty('vote_average', Number, true)
  public voteAverage: number | undefined = undefined;

  @JsonProperty('vote_count', Number, true)
  public voteCount: number | undefined = undefined;

  public getBackdrop(size: ImageAPISizesEnum = ImageAPISizesEnum.W500): string {
    return this.backdropPath ? `${CONFIG.APIS.MOVIE_API.IMAGE_RESOURCES_URL}/${size}${this.backdropPath}` : '';
  }

  public getPoster(size: ImageAPISizesEnum = ImageAPISizesEnum.W500): string {
    return this.posterPath ? `${CONFIG.APIS.MOVIE_API.IMAGE_RESOURCES_URL}/${size}${this.posterPath}` : '';
  }

  [propName: string]: string | string[] | any;
}
