import {JsonObject, JsonProperty} from 'json2typescript';
import {ISODateConverter} from '../../convertors/iso-date.convertor';
import {CONFIG} from '../../../config/config';
import {ImageAPISizesEnum} from './index';

@JsonObject('MovieDetailsModel')
export default class MovieDetailsModel {
  @JsonProperty('id', Number, true)
  public id: number | undefined = undefined;

  @JsonProperty('popularity', Number, true)
  public popularity: number | undefined = undefined;

  @JsonProperty('vote_count', Number, true)
  public voteCount: number | undefined = undefined;

  @JsonProperty('video', Boolean, true)
  public video = false;

  @JsonProperty('poster_path', String, true)
  public posterPath: string | undefined = undefined;

  @JsonProperty('adult', Boolean, true)
  public adult = true;

  @JsonProperty('backdrop_path', String, true)
  public backdropPath: string | undefined = undefined;

  @JsonProperty('original_language', String, true)
  public originalLanguage: string | undefined = undefined;

  @JsonProperty('original_title', String, true)
  public originalTitle: string | undefined = undefined;

  @JsonProperty('genre_ids', [Number], true)
  public genreIds: number[] = [];

  @JsonProperty('title', String, true)
  public title: string | undefined = undefined;

  @JsonProperty('vote_average', Number, true)
  public voteAverage: number | undefined = undefined;

  @JsonProperty('overview', String, true)
  public overview: string | undefined = undefined;

  @JsonProperty('release_date', ISODateConverter, true)
  public releaseDate: Date | undefined = undefined;

  [propName: string]: string | string[] | any;

  public getBackdrop(size: ImageAPISizesEnum = ImageAPISizesEnum.W500): string {
    return this.backdropPath ? `${CONFIG.APIS.MOVIE_API.IMAGE_RESOURCES_URL}/${size}${this.backdropPath}` : '';
  }

  public getPoster(size: ImageAPISizesEnum = ImageAPISizesEnum.W500): string {
    return this.posterPath ? `${CONFIG.APIS.MOVIE_API.IMAGE_RESOURCES_URL}/${size}${this.posterPath}` : '';
  }
}

