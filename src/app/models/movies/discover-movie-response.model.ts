import MovieDetailsModel from './movie.model';
import { JsonObject, JsonProperty } from 'json2typescript';


@JsonObject('DiscoverMovieResponse')
export default class DiscoverMovieResponse {
  @JsonProperty('page', Number, true)
  public page: number = 1;

  @JsonProperty('total_results', Number, true)
  public totalResults: number = 0;

  @JsonProperty('total_pages', Number, true)
  public totalPages: number = 0;

  @JsonProperty('results', [MovieDetailsModel], true)
  public results: MovieDetailsModel[] = [];

  [propName: string]: string | string[] | any;
}


// "page":1,
//   "total_results":10000,
//   "total_pages":500,
//   "results"
