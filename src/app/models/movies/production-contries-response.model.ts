import { JsonObject, JsonProperty } from 'json2typescript';

@JsonObject('ProductionCountriesResponseModel')
export default class ProductionCountriesResponseModel {
  @JsonProperty('iso_3166_1', String, true)
  public iso: string | undefined = '';

  @JsonProperty('name', String, true)
  public name: string | undefined  = '';

  [propName: string]: string | string[] | any;
}
