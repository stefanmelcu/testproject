import {deserialize} from '../../../utils/deserializer';
import SpokenLanguagesModel from './spoken-languages.model';

describe('SpokenLanguagesModel', () => {
  const mockRowModel: any =   {
    iso_639_1: 'testIOS',
    name: 'en'
  };
  let model: any;

  beforeEach(() => {
    model = undefined;
    model = deserialize(
      mockRowModel,
      SpokenLanguagesModel,
      { debug: false }
    );
  });

  it('should correctly deserialize the object', () => {
    expect(model instanceof SpokenLanguagesModel).toBeTruthy();
  });
});

