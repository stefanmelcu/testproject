import { JsonObject, JsonProperty } from 'json2typescript';

@JsonObject('SpokenLanguagesModel')
export default class SpokenLanguagesModel {
  @JsonProperty('iso_639_1', String, true)
  public iso: string | undefined = '';

  @JsonProperty('name', String, true)
  public name: string | undefined  = '';

  [propName: string]: string | string[] | any;
}
