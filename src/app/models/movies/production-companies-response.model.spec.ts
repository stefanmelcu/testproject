import {deserialize} from '../../../utils/deserializer';
import ProductionCompaniesResponseModel from './production-companies-response.model';
import {ImageAPISizesEnum} from './index';
import {CONFIG} from '../../../config/config';

describe('ProductionCompaniesResponseModel', () => {
  const mockRowModel: any =   {
    id: 508,
    logo_path: '/7PzJdsLGlR7oW4J0J5Xcd0pHGRg.png',
    name: 'Regency Enterprises',
    origin_country: 'US'
  };
  let model: any;

  beforeEach(() => {
    model = undefined;
    model = deserialize(
      mockRowModel,
      ProductionCompaniesResponseModel,
      { debug: false }
    );
  });

  it('should correctly deserialize the object', () => {
    expect(model instanceof ProductionCompaniesResponseModel).toBeTruthy();
  });

  it('should correctly get the logo', () => {
    expect((model as ProductionCompaniesResponseModel).getLogoPath(ImageAPISizesEnum.W100)).toBe(
      `${CONFIG.APIS.MOVIE_API.IMAGE_RESOURCES_URL}/w100${(model as ProductionCompaniesResponseModel).logoPath}`
    );
  });
});

