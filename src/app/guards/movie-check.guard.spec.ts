import { TestBed } from '@angular/core/testing';

import { MovieCheckGuard } from './movie-check.guard';
import {Route, Router} from '@angular/router';
import {MockStore, provideMockStore} from '@ngrx/store/testing';
import {MovieDetailsStatePhaseEnum} from '../store/reducers/movie.reducers';
import DetailsMovieResponse from '../models/movies/details-movie-response.model';
import {MoviesStatePhaseEnum} from '../store/reducers/movies.reducers';
import DiscoverMovieResponse from '../models/movies/discover-movie-response.model';
import {movieDetailsResponseKey, moviesListResponseKey, RootStoreState} from '../store/reducers';
import {loadMovieAction} from '../store/actions/movies.actions';

describe('MovieCheckGuard', () => {
  let guard: MovieCheckGuard;
  let store: MockStore;
  const fakeRootState: RootStoreState = {
    [moviesListResponseKey]: {
      moviesPhase: MoviesStatePhaseEnum.INIT,
      moviesListResponse: new DiscoverMovieResponse(),
      error: 'good'
    },
    [movieDetailsResponseKey]: {
      moviePhase: MovieDetailsStatePhaseEnum.INIT,
      movieDetails: new DetailsMovieResponse(),
      error: 'baaaad'
    }
  };
  let activatedRouteSnapshotStub: any;
  let routerStateSnapshotStub: any;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MovieCheckGuard,
        provideMockStore({ initialState: fakeRootState }),
        {
          provide: Router,
          useValue: jasmine.createSpyObj('Router', [ 'navigateByUrl' ])
        }
      ]
    });
    activatedRouteSnapshotStub = {
      params: {id: 123}
    };
    routerStateSnapshotStub = {};

    store = TestBed.inject(MockStore);
    guard = TestBed.inject(MovieCheckGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });

  it('should should return true if the parameters are correct', () => {
    const storeSpy = spyOn((guard as any).store, 'dispatch');

    expect(guard.canActivate(activatedRouteSnapshotStub, routerStateSnapshotStub)).toBe(true);
    expect(storeSpy).toHaveBeenCalledWith(loadMovieAction({ id: activatedRouteSnapshotStub.params.id }));
  });

  it('should redirect if the parameters are incorrect', () => {
    const router = TestBed.inject(Router);

    guard.canActivate({
        params: {id: 'sss'}
      } as any,
      routerStateSnapshotStub
    );
    expect(router.navigateByUrl).toHaveBeenCalledWith('404');
  });
});
