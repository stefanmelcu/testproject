import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {RootStoreState} from '../store/reducers';
import {Store} from '@ngrx/store';
import {loadMovieAction} from '../store/actions/movies.actions';
import {CONFIG} from '../../config/config';

@Injectable({
  providedIn: 'root'
})
export class MovieCheckGuard implements CanActivate {
  constructor(
    private store: Store<RootStoreState>,
    private router: Router
) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const id: number = Number.parseInt(next.params.id || undefined, 10);

    if (!id || isNaN(id)) {
      this.router.navigateByUrl(CONFIG.ROUTES.NOT_FOUND.SLUG);
      return false;
    }

    // INFO we initialize the request for the Movie details before we enter the page.
    // This can also work in a Resolver apart form a Guard
    this.store.dispatch(loadMovieAction({ id }));
    return true;
  }
}
